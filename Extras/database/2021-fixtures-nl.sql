-----------------------------------------------------------------------------
-- De landen die deelnemen aan Euro 2020 (gespeeld in 2021)                --
-----------------------------------------------------------------------------

INSERT INTO Countries VALUES ('tr','Turkije','A');
INSERT INTO Countries VALUES ('it','Italië','A');
INSERT INTO Countries VALUES ('wa','Wales','A');
INSERT INTO Countries VALUES ('ch','Zwitserland','A');

INSERT INTO Countries VALUES ('dk','Denemarken','B');
INSERT INTO Countries VALUES ('fi','Finland','B');
INSERT INTO Countries VALUES ('be','België','B');
INSERT INTO Countries VALUES ('ru','Rusland','B');

INSERT INTO Countries VALUES ('nl','Nederland','C');
INSERT INTO Countries VALUES ('ua','Oekraïne','C');
INSERT INTO Countries VALUES ('at','Oostenrijk','C');
INSERT INTO Countries VALUES ('mk','Noord-Macedonië','C');

INSERT INTO Countries VALUES ('en','Engeland','D');
INSERT INTO Countries VALUES ('hr','Kroatië','D');
INSERT INTO Countries VALUES ('sc','Schotland','D');
INSERT INTO Countries VALUES ('cz','Tsjechië','D');

INSERT INTO Countries VALUES ('es','Spanje','E');
INSERT INTO Countries VALUES ('se','Zweden','E');
INSERT INTO Countries VALUES ('pl','Polen','E');
INSERT INTO Countries VALUES ('sk','Slowakije','E');

INSERT INTO Countries VALUES ('hu','Hongarije','F');
INSERT INTO Countries VALUES ('pt','Portugal','F');
INSERT INTO Countries VALUES ('fr','Frankrijk','F');
INSERT INTO Countries VALUES ('de','Duitsland','F');

-----------------------------------------------------------------------------
-- De fases die het toernooi heeft                                         --
-----------------------------------------------------------------------------

INSERT INTO Stages VALUES (1,'Groepsfase',0,36,1,1);
INSERT INTO Stages VALUES (2,'Achtste finales',1,16,2,2);
INSERT INTO Stages VALUES (3,'Kwartfinales',1,8,4,3);
INSERT INTO Stages VALUES (4,'Halve finales',1,4,7,4);
INSERT INTO Stages VALUES (5,'Finale',1,2,10,5);
INSERT INTO Stages VALUES (6,'Kampioen',1,1,20,6);

-----------------------------------------------------------------------------
-- De wedstrijden die gespeeld worden                                      --
-----------------------------------------------------------------------------

--- Groep A
INSERT INTO Matches VALUES ( 1,1,'A','tr','it','2021-06-11 21:00:00',NULL,'Stadio Olimpico, Rome');
INSERT INTO Matches VALUES ( 2,1,'A','wa','ch','2021-06-12 15:00:00',NULL,'Olympisch Stadion, Bakoe');
INSERT INTO Matches VALUES (13,1,'A','tr','wa','2021-06-16 18:00:00',NULL,'Olympisch Stadion, Bakoe');
INSERT INTO Matches VALUES (14,1,'A','it','ch','2021-06-16 21:00:00',NULL,'Stadio Olimpico, Rome');
INSERT INTO Matches VALUES (25,1,'A','ch','tr','2021-06-20 18:00:00',NULL,'Olympisch Stadion, Bakoe');
INSERT INTO Matches VALUES (26,1,'A','it','wa','2021-06-20 18:00:00',NULL,'Stadio Olimpico, Rome');

--- Groep B
INSERT INTO Matches VALUES ( 3,1,'B','dk','fi','2021-06-12 18:00:00',NULL,'Parken, Kopenhagen');
INSERT INTO Matches VALUES ( 4,1,'B','be','ru','2021-06-12 21:00:00',NULL,'Krestovskistadion, Sint-Petersburg');
INSERT INTO Matches VALUES (15,1,'B','fi','ru','2021-06-16 15:00:00',NULL,'Krestovskistadion, Sint-Petersburg');
INSERT INTO Matches VALUES (16,1,'B','dk','be','2021-06-17 18:00:00',NULL,'Parken, Kopenhagen');
INSERT INTO Matches VALUES (27,1,'B','ru','dk','2021-06-21 21:00:00',NULL,'Parken, Kopenhagen');
INSERT INTO Matches VALUES (28,1,'B','fi','be','2021-06-21 21:00:00',NULL,'Krestovskistadion, Sint-Petersburg');

--- Groep C
INSERT INTO Matches VALUES ( 6,1,'C','at','mk','2021-06-13 18:00:00',NULL,'Arena Națională, Boekarest');
INSERT INTO Matches VALUES ( 5,1,'C','nl','ua','2021-06-13 21:00:00',NULL,'Johan Cruijff ArenA, Amsterdam');
INSERT INTO Matches VALUES (18,1,'C','ua','mk','2021-06-17 15:00:00',NULL,'Arena Națională, Boekarest');
INSERT INTO Matches VALUES (17,1,'C','nl','at','2021-06-17 21:00:00',NULL,'Johan Cruijff ArenA, Amsterdam');
INSERT INTO Matches VALUES (29,1,'C','mk','nl','2021-06-21 18:00:00',NULL,'Johan Cruijff ArenA, Amsterdam');
INSERT INTO Matches VALUES (30,1,'C','ua','at','2021-06-21 18:00:00',NULL,'Arena Națională, Boekarest');

--- Groep D
INSERT INTO Matches VALUES ( 7,1,'D','en','hr','2021-06-13 15:00:00',NULL,'Wembley Stadium, Londen');
INSERT INTO Matches VALUES ( 8,1,'D','sc','cz','2021-06-14 15:00:00',NULL,'Hampden Park, Glasgow');
INSERT INTO Matches VALUES (19,1,'D','hr','cz','2021-06-18 18:00:00',NULL,'Hampden Park, Glasgow');
INSERT INTO Matches VALUES (20,1,'D','en','sc','2021-06-18 21:00:00',NULL,'Wembley Stadium, Londen');
INSERT INTO Matches VALUES (31,1,'D','hr','sc','2021-06-22 21:00:00',NULL,'Hampden Park, Glasgow');
INSERT INTO Matches VALUES (32,1,'D','cz','en','2021-06-22 21:00:00',NULL,'Wembley Stadium, Londen');

--- Groep E
INSERT INTO Matches VALUES (10,1,'E','pl','sk','2021-06-14 18:00:00',NULL,'Avivastadion, Dublin');
INSERT INTO Matches VALUES ( 9,1,'E','es','se','2021-06-14 21:00:00',NULL,'San Mamés, Bilbao');
INSERT INTO Matches VALUES (21,1,'E','se','sk','2021-06-18 15:00:00',NULL,'Avivastadion, Dublin');
INSERT INTO Matches VALUES (22,1,'E','es','pl','2021-06-19 21:00:00',NULL,'San Mamés, Bilbao');
INSERT INTO Matches VALUES (33,1,'E','sk','es','2021-06-23 18:00:00',NULL,'San Mamés, Bilbao');
INSERT INTO Matches VALUES (34,1,'E','se','pl','2021-06-23 18:00:00',NULL,'Avivastadion, Dublin');

--- Groep F
INSERT INTO Matches VALUES (11,1,'F','hu','pt','2021-06-15 18:00:00',NULL,'Puskás Aréna, Boedapest');
INSERT INTO Matches VALUES (12,1,'F','fr','de','2021-06-15 21:00:00',NULL,'Allianz Arena, München');
INSERT INTO Matches VALUES (23,1,'F','hu','fr','2021-06-19 15:00:00',NULL,'Puskás Aréna, Boedapest');
INSERT INTO Matches VALUES (24,1,'F','pt','de','2021-06-19 18:00:00',NULL,'Allianz Arena, München');
INSERT INTO Matches VALUES (35,1,'F','pt','fr','2021-06-23 21:00:00',NULL,'Puskás Aréna, Boedapest');
INSERT INTO Matches VALUES (36,1,'F','de','hu','2021-06-23 21:00:00',NULL,'Allianz Arena, München');

--- Knockout - Achtste finales
INSERT INTO Matches VALUES (37,2,NULL,NULL,NULL,'2021-06-26 21:00:00',NULL,'Wembley Stadium, Londen');
INSERT INTO Matches VALUES (38,2,NULL,NULL,NULL,'2021-06-26 18:00:00',NULL,'Johan Cruijff ArenA, Amsterdam');
INSERT INTO Matches VALUES (39,2,NULL,NULL,NULL,'2021-06-27 21:00:00',NULL,'San Mamés, Bilbao');
INSERT INTO Matches VALUES (40,2,NULL,NULL,NULL,'2021-06-27 18:00:00',NULL,'Puskás Aréna, Boedapest');
INSERT INTO Matches VALUES (41,2,NULL,NULL,NULL,'2020-06-28 21:00:00',NULL,'Arena Națională, Boekarest');
INSERT INTO Matches VALUES (42,2,NULL,NULL,NULL,'2020-06-28 18:00:00',NULL,'Parken, Kopenhagen');
INSERT INTO Matches VALUES (43,2,NULL,NULL,NULL,'2020-06-29 21:00:00',NULL,'Hampden Park, Glasgow');
INSERT INTO Matches VALUES (44,2,NULL,NULL,NULL,'2020-06-29 18:00:00',NULL,'Avivastadion, Dublin');

--- Knockout - Kwartfinales
INSERT INTO Matches VALUES (45,3,NULL,NULL,NULL,'2020-07-02 18:00:00',NULL,'Krestovskistadion, Sint-Petersburg');
INSERT INTO Matches VALUES (46,3,NULL,NULL,NULL,'2020-07-02 21:00:00',NULL,'Allianz Arena, München');
INSERT INTO Matches VALUES (47,3,NULL,NULL,NULL,'2020-07-03 18:00:00',NULL,'Olympisch Stadion, Bakoe');
INSERT INTO Matches VALUES (48,3,NULL,NULL,NULL,'2020-07-03 21:00:00',NULL,'Stadio Olimpico, Rome');

--- Knockout - Halve finales
INSERT INTO Matches VALUES (49,4,NULL,NULL,NULL,'2020-07-06 21:00:00',NULL,'Wembley Stadium, Londen');
INSERT INTO Matches VALUES (50,4,NULL,NULL,NULL,'2020-07-07 21:00:00',NULL,'Wembley Stadium, Londen');

--- Knockout - Finale
INSERT INTO Matches VALUES (51,5,NULL,NULL,NULL,'2020-07-11 21:00:00',NULL,'Wembley Stadium, Londen');

--- Kampioen
INSERT INTO Matches VALUES (52,6,NULL,NULL,NULL,NULL,NULL,NULL);
