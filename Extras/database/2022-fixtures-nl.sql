-----------------------------------------------------------------------------
-- De landen die deelnemen aan het WK 2022                                 --
-----------------------------------------------------------------------------

INSERT INTO Countries VALUES ('qa','Qatar','A');
INSERT INTO Countries VALUES ('ec','Ecuador','A');
INSERT INTO Countries VALUES ('sn','Senegal','A');
INSERT INTO Countries VALUES ('nl','Nederland','A');

INSERT INTO Countries VALUES ('en','Engeland','B');
INSERT INTO Countries VALUES ('ir','Iran','B');
INSERT INTO Countries VALUES ('us','Verenigde Staten','B');
INSERT INTO Countries VALUES ('wa','Wales','B');

INSERT INTO Countries VALUES ('ar','Argentinië','C');
INSERT INTO Countries VALUES ('sa','Saudi-Arabië','C');
INSERT INTO Countries VALUES ('mx','Mexico','C');
INSERT INTO Countries VALUES ('pl','Polen','C');

INSERT INTO Countries VALUES ('fr','Frankrijk','D');
INSERT INTO Countries VALUES ('au','Australië','D');
INSERT INTO Countries VALUES ('dk','Denemarken','D');
INSERT INTO Countries VALUES ('tn','Tunesië','D');

INSERT INTO Countries VALUES ('es','Spanje','E');
INSERT INTO Countries VALUES ('cr','Costa Rica','E');
INSERT INTO Countries VALUES ('de','Duitsland','E');
INSERT INTO Countries VALUES ('jp','Japan','E');

INSERT INTO Countries VALUES ('be','België','F');
INSERT INTO Countries VALUES ('ca','Canada','F');
INSERT INTO Countries VALUES ('ma','Marokko','F');
INSERT INTO Countries VALUES ('hr','Kroatië','F');

INSERT INTO Countries VALUES ('br','Brazilië','G');
INSERT INTO Countries VALUES ('rs','Servië','G');
INSERT INTO Countries VALUES ('ch','Zwitserland','G');
INSERT INTO Countries VALUES ('cm','Kameroen','G');

INSERT INTO Countries VALUES ('pt','Portugal','H');
INSERT INTO Countries VALUES ('gh','Ghana','H');
INSERT INTO Countries VALUES ('uy','Uruguay','H');
INSERT INTO Countries VALUES ('kr','Zuid-Korea','H');

-----------------------------------------------------------------------------
-- De fases die het toernooi heeft                                         --
-----------------------------------------------------------------------------

INSERT INTO Stages VALUES (1,'Groepsfase',0,48,1);
INSERT INTO Stages VALUES (2,'Achtste finales',1,16,2);
INSERT INTO Stages VALUES (3,'Kwartfinales',1,8,4);
INSERT INTO Stages VALUES (4,'Halve finales',1,4,7);
INSERT INTO Stages VALUES (5,'Finale',1,2,10);
INSERT INTO Stages VALUES (6,'Kampioen',1,1,20);

-----------------------------------------------------------------------------
-- De wedstrijden die gespeeld worden                                      --
-----------------------------------------------------------------------------

--- Groep A
INSERT INTO Matches VALUES ( 1, 1,'A','qa','ec','2022-11-20 17:00',NULL,NULL,NULL,'Al Bayt Stadion, Al Khor');
INSERT INTO Matches VALUES ( 2, 1,'A','sn','nl','2022-11-21 17:00',NULL,NULL,NULL,'Al Thumama Stadion, Doha');
INSERT INTO Matches VALUES (18, 1,'A','qa','sn','2022-11-25 14:00',NULL,NULL,NULL,'Al Thumama Stadion, Doha');
INSERT INTO Matches VALUES (19, 1,'A','nl','ec','2022-11-25 17:00',NULL,NULL,NULL,'Khalifa International Stadion, Al Rayyan');
INSERT INTO Matches VALUES (35, 1,'A','ec','sn','2022-11-29 16:00',NULL,NULL,NULL,'Khalifa International Stadion, Al Rayyan');
INSERT INTO Matches VALUES (36, 1,'A','nl','qa','2022-11-29 16:00',NULL,NULL,NULL,'Al Bayt Stadion, Al Khor');

--- Groep B
INSERT INTO Matches VALUES ( 3, 1,'B','en','ir','2022-11-21 14:00',NULL,NULL,NULL,'Khalifa International Stadion, Al Rayyan');
INSERT INTO Matches VALUES ( 4, 1,'B','us','wa','2022-11-21 20:00',NULL,NULL,NULL,'Ahmed Bin Ali Stadion, Al Rayyan');
INSERT INTO Matches VALUES (17, 1,'B','wa','ir','2022-11-25 11:00',NULL,NULL,NULL,'Ahmed Bin Ali Stadion, Al Rayyan');
INSERT INTO Matches VALUES (20, 1,'B','en','us','2022-11-25 20:00',NULL,NULL,NULL,'Al Bayt Stadion, Al Khor');
INSERT INTO Matches VALUES (33, 1,'B','wa','en','2022-11-29 20:00',NULL,NULL,NULL,'Ahmed Bin Ali Stadion, Al Rayyan');
INSERT INTO Matches VALUES (34, 1,'B','ir','us','2022-11-29 20:00',NULL,NULL,NULL,'Al Thumama Stadion, Doha');

--- Groep C
INSERT INTO Matches VALUES ( 8, 1,'C','ar','sa','2022-11-22 11:00',NULL,NULL,NULL,'Lusail Iconic Stadion, Lusail');
INSERT INTO Matches VALUES ( 7, 1,'C','mx','pl','2022-11-22 17:00',NULL,NULL,NULL,'Stadium 974, Doha');
INSERT INTO Matches VALUES (22, 1,'C','pl','sa','2022-11-26 14:00',NULL,NULL,NULL,'Education City Stadion, Al Rayyan');
INSERT INTO Matches VALUES (24, 1,'C','ar','mx','2022-11-26 20:00',NULL,NULL,NULL,'Lusail Iconic Stadion, Lusail');
INSERT INTO Matches VALUES (39, 1,'C','pl','ar','2022-11-30 20:00',NULL,NULL,NULL,'Stadium 974, Doha');
INSERT INTO Matches VALUES (40, 1,'C','sa','mx','2022-11-30 20:00',NULL,NULL,NULL,'Lusail Iconic Stadion, Lusail');

--- Groep D
INSERT INTO Matches VALUES ( 6, 1,'D','dk','tn','2022-11-22 14:00',NULL,NULL,NULL,'Education City Stadion, Al Rayyan');
INSERT INTO Matches VALUES ( 5, 1,'D','fr','au','2022-11-22 20:00',NULL,NULL,NULL,'Al Janoub Stadion, Al Wakrah');
INSERT INTO Matches VALUES (21, 1,'D','tn','au','2022-11-26 11:00',NULL,NULL,NULL,'Al Janoub Stadion, Al Wakrah');
INSERT INTO Matches VALUES (23, 1,'D','fr','dk','2022-11-26 17:00',NULL,NULL,NULL,'Stadium 974, Doha');
INSERT INTO Matches VALUES (37, 1,'D','au','dk','2022-11-30 16:00',NULL,NULL,NULL,'Al Janoub Stadion, Al Wakrah');
INSERT INTO Matches VALUES (38, 1,'D','tn','fr','2022-11-30 16:00',NULL,NULL,NULL,'Education City Stadion, Al Rayyan');

--- Groep E
INSERT INTO Matches VALUES (11, 1,'E','de','jp','2022-11-23 14:00',NULL,NULL,NULL,'Khalifa International Stadion, Al Rayyan');
INSERT INTO Matches VALUES (10, 1,'E','es','cr','2022-11-23 17:00',NULL,NULL,NULL,'Al Thumama Stadion, Doha');
INSERT INTO Matches VALUES (25, 1,'E','jp','cr','2022-11-27 11:00',NULL,NULL,NULL,'Ahmed Bin Ali Stadion, Al Rayyan');
INSERT INTO Matches VALUES (28, 1,'E','es','de','2022-11-27 20:00',NULL,NULL,NULL,'Al Bayt Stadion, Al Khor');
INSERT INTO Matches VALUES (43, 1,'E','jp','es','2022-12-01 20:00',NULL,NULL,NULL,'Khalifa International Stadion, Al Rayyan');
INSERT INTO Matches VALUES (44, 1,'E','cr','de','2022-12-01 20:00',NULL,NULL,NULL,'Al Bayt Stadion, Al Khor');

--- Groep F
INSERT INTO Matches VALUES (12, 1,'F','ma','hr','2022-11-23 11:00',NULL,NULL,NULL,'Al Bayt Stadion, Al Khor');
INSERT INTO Matches VALUES ( 9, 1,'F','be','ca','2022-11-23 20:00',NULL,NULL,NULL,'Ahmed Bin Ali Stadion, Al Rayyan');
INSERT INTO Matches VALUES (26, 1,'F','be','ma','2022-11-27 14:00',NULL,NULL,NULL,'Al Thumama Stadion, Doha');
INSERT INTO Matches VALUES (27, 1,'F','hr','ca','2022-11-27 17:00',NULL,NULL,NULL,'Khalifa International Stadion, Al Rayyan');
INSERT INTO Matches VALUES (41, 1,'F','hr','be','2022-12-01 16:00',NULL,NULL,NULL,'Ahmed Bin Ali Stadion, Al Rayyan');
INSERT INTO Matches VALUES (42, 1,'F','ca','ma','2022-12-01 16:00',NULL,NULL,NULL,'Al Thumama Stadion, Doha');

--- Groep G
INSERT INTO Matches VALUES (13, 1,'G','ch','cm','2022-11-24 11:00',NULL,NULL,NULL,'Al Janoub Stadion, Al Wakrah');
INSERT INTO Matches VALUES (16, 1,'G','br','rs','2022-11-24 20:00',NULL,NULL,NULL,'Lusail Iconic Stadion, Lusail');
INSERT INTO Matches VALUES (29, 1,'G','cm','rs','2022-11-28 11:00',NULL,NULL,NULL,'Al Janoub Stadion, Al Wakrah');
INSERT INTO Matches VALUES (31, 1,'G','br','ch','2022-11-28 17:00',NULL,NULL,NULL,'Stadium 974, Doha');
INSERT INTO Matches VALUES (47, 1,'G','rs','ch','2022-12-02 20:00',NULL,NULL,NULL,'Stadium 974, Doha');
INSERT INTO Matches VALUES (48, 1,'G','cm','br','2022-12-02 20:00',NULL,NULL,NULL,'Lusail Iconic Stadion, Lusail');

--- Groep H
INSERT INTO Matches VALUES (14, 1,'H','uy','kr','2022-11-24 14:00',NULL,NULL,NULL,'Education City Stadion, Al Rayyan');
INSERT INTO Matches VALUES (15, 1,'H','pt','gh','2022-11-24 17:00',NULL,NULL,NULL,'Stadium 974, Doha');
INSERT INTO Matches VALUES (30, 1,'H','kr','gh','2022-11-28 14:00',NULL,NULL,NULL,'Education City Stadion, Al Rayyan');
INSERT INTO Matches VALUES (32, 1,'H','pt','uy','2022-11-28 20:00',NULL,NULL,NULL,'Lusail Iconic Stadion, Lusail');
INSERT INTO Matches VALUES (45, 1,'H','gh','uy','2022-12-02 16:00',NULL,NULL,NULL,'Al Janoub Stadion, Al Wakrah');
INSERT INTO Matches VALUES (46, 1,'H','kr','pt','2022-12-02 16:00',NULL,NULL,NULL,'Education City Stadion, Al Rayyan');

--- Knockout - Achtste finales
INSERT INTO Matches VALUES (49,2,NULL,NULL,NULL,'2022-12-03 16:00',NULL,'1A','2B','Khalifa International Stadion, Al Rayyan');
INSERT INTO Matches VALUES (50,2,NULL,NULL,NULL,'2022-12-03 20:00',NULL,'1C','2D','Ahmed Bin Ali Stadion, Al Rayyan');
INSERT INTO Matches VALUES (52,2,NULL,NULL,NULL,'2022-12-04 16:00',NULL,'1D','2C','Al Thumama Stadion, Doha');
INSERT INTO Matches VALUES (51,2,NULL,NULL,NULL,'2022-12-04 20:00',NULL,'1B','2A','Al Bayt Stadion, Al Khor');
INSERT INTO Matches VALUES (53,2,NULL,NULL,NULL,'2022-12-05 16:00',NULL,'1E','2F','Al Janoub Stadion, Al Wakrah');
INSERT INTO Matches VALUES (54,2,NULL,NULL,NULL,'2022-12-05 20:00',NULL,'1G','2H','Stadium 974, Doha');
INSERT INTO Matches VALUES (55,2,NULL,NULL,NULL,'2022-12-06 16:00',NULL,'1F','2E','Education City Stadion, Al Rayyan');
INSERT INTO Matches VALUES (56,2,NULL,NULL,NULL,'2022-12-06 20:00',NULL,'1H','2G','Lusail Iconic Stadion, Lusail');

--- Knockout - Kwartfinales
INSERT INTO Matches VALUES (58,3,NULL,NULL,NULL,'2022-12-09 16:00',NULL,'W53','W54','Education City Stadion, Al Rayyan');
INSERT INTO Matches VALUES (57,3,NULL,NULL,NULL,'2022-12-09 20:00',NULL,'W49','W50','Lusail Iconic Stadion, Lusail');
INSERT INTO Matches VALUES (60,3,NULL,NULL,NULL,'2022-12-10 16:00',NULL,'W55','W56','Al Thumama Stadion, Doha');
INSERT INTO Matches VALUES (59,3,NULL,NULL,NULL,'2022-12-10 20:00',NULL,'W51','W52','Al Bayt Stadion, Al Khor');

--- Knockout - Halve finales
INSERT INTO Matches VALUES (61,4,NULL,NULL,NULL,'2022-12-13 20:00',NULL,'W57','W58','Lusail Iconic Stadion, Lusail');
INSERT INTO Matches VALUES (62,4,NULL,NULL,NULL,'2022-12-14 20:00',NULL,'W59','W60','Al Bayt Stadion, Al Khor');

--- Knockout - Finale
INSERT INTO Matches VALUES (63,5,NULL,NULL,NULL,'2022-12-18 16:00',NULL,'W61','W62','Lusail Iconic Stadion, Lusail');

--- Kampioen
INSERT INTO Matches VALUES (64,6,NULL,NULL,NULL,NULL,NULL,'W63',NULL,NULL);
