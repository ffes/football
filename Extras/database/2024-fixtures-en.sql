-----------------------------------------------------------------------------
-- Euro 2024 played in Germany                                             --
-----------------------------------------------------------------------------
-- The stages the tournament has                                           --
-----------------------------------------------------------------------------

INSERT INTO Stages VALUES (1,'Group Stage',0,36,1,1);
INSERT INTO Stages VALUES (2,'Round of 16',1,16,2,2);
INSERT INTO Stages VALUES (3,'Quarter-finals',1,8,4,3);
INSERT INTO Stages VALUES (4,'Semi-finals',1,4,7,4);
INSERT INTO Stages VALUES (5,'Final',1,2,10,5);
INSERT INTO Stages VALUES (6,'Champion',1,1,20,6);

-----------------------------------------------------------------------------
-- The countries that take part                                            --
-- The matches played in the tournament                                    --
-----------------------------------------------------------------------------

--- Group A
INSERT INTO Countries VALUES ('ch','Switserland','A');
INSERT INTO Countries VALUES ('de','Germany','A');
INSERT INTO Countries VALUES ('hu','Hungary','A');
INSERT INTO Countries VALUES ('sc','Scotland','A');

INSERT INTO Matches VALUES ( 1,1,'A','de','sc','2024-06-14 21:00',NULL,NULL,NULL,'Allianz Arena, Munich');
INSERT INTO Matches VALUES ( 2,1,'A','hu','ch','2024-06-15 15:00',NULL,NULL,NULL,'RheinEnergieStadium, Cologne');
INSERT INTO Matches VALUES (13,1,'A','sc','ch','2024-06-19 21:00',NULL,NULL,NULL,'RheinEnergieStadium, Cologne');
INSERT INTO Matches VALUES (14,1,'A','de','hu','2024-06-19 18:00',NULL,NULL,NULL,'MHPArena, Stuttgart');
INSERT INTO Matches VALUES (25,1,'A','ch','de','2024-06-23 21:00',NULL,NULL,NULL,'Deutsche Bank Park, Frankfurt am Main');
INSERT INTO Matches VALUES (26,1,'A','sc','hu','2024-06-23 21:00',NULL,NULL,NULL,'MHPArena, Stuttgart');

--- Group B
INSERT INTO Countries VALUES ('al','Albania','B');
INSERT INTO Countries VALUES ('es','Spain','B');
INSERT INTO Countries VALUES ('hr','Croatia','B');
INSERT INTO Countries VALUES ('it','Italy','B');

INSERT INTO Matches VALUES ( 3,1,'B','es','hr','2024-06-15 18:00',NULL,NULL,NULL,'Olympiastadium, Berlin');
INSERT INTO Matches VALUES ( 4,1,'B','it','al','2024-06-15 21:00',NULL,NULL,NULL,'Signal Iduna Park, Dortmund');
INSERT INTO Matches VALUES (15,1,'B','hr','al','2024-06-19 15:00',NULL,NULL,NULL,'Volksparkstadium, Hamburg');
INSERT INTO Matches VALUES (16,1,'B','es','it','2024-06-20 21:00',NULL,NULL,NULL,'Veltins-Arena, Gelsenkirchen');
INSERT INTO Matches VALUES (27,1,'B','al','es','2024-06-24 21:00',NULL,NULL,NULL,'Merkur Spiel-Arena, Düsseldorf');
INSERT INTO Matches VALUES (28,1,'B','hr','it','2024-06-24 21:00',NULL,NULL,NULL,'Red Bull Arena, Leipzig');

--- Group C
INSERT INTO Countries VALUES ('dk','Denmark','C');
INSERT INTO Countries VALUES ('en','England','C');
INSERT INTO Countries VALUES ('rs','Serbia','C');
INSERT INTO Countries VALUES ('si','Slovenia','C');

INSERT INTO Matches VALUES ( 5,1,'C','rs','en','2024-06-16 21:00',NULL,NULL,NULL,'Veltins-Arena, Gelsenkirchen');
INSERT INTO Matches VALUES ( 6,1,'C','si','dk','2024-06-16 18:00',NULL,NULL,NULL,'MHPArena, Stuttgart');
INSERT INTO Matches VALUES (17,1,'C','dk','en','2024-06-20 18:00',NULL,NULL,NULL,'Deutsche Bank Park, Frankfurt am Main');
INSERT INTO Matches VALUES (18,1,'C','si','rs','2024-06-20 15:00',NULL,NULL,NULL,'Allianz Arena, Munich');
INSERT INTO Matches VALUES (29,1,'C','en','si','2024-06-25 21:00',NULL,NULL,NULL,'RheinEnergieStadium, Cologne');
INSERT INTO Matches VALUES (30,1,'C','dk','rs','2024-06-25 21:00',NULL,NULL,NULL,'Allianz Arena, Munich');

--- Group D
INSERT INTO Countries VALUES ('at','Austria','D');
INSERT INTO Countries VALUES ('fr','France','D');
INSERT INTO Countries VALUES ('nl','Netherlands','D');
INSERT INTO Countries VALUES ('pl','Poland','D');

INSERT INTO Matches VALUES ( 7,1,'D','pl','nl','2024-06-16 15:00',NULL,NULL,NULL,'Volksparkstadium, Hamburg');
INSERT INTO Matches VALUES ( 8,1,'D','at','fr','2024-06-17 21:00',NULL,NULL,NULL,'Merkur Spiel-Arena, Düsseldorf');
INSERT INTO Matches VALUES (19,1,'D','pl','at','2024-06-21 18:00',NULL,NULL,NULL,'Olympia Stadium, Berlin');
INSERT INTO Matches VALUES (20,1,'D','nl','fr','2024-06-21 21:00',NULL,NULL,NULL,'Red Bull Arena, Leipzig');
INSERT INTO Matches VALUES (31,1,'D','nl','at','2024-06-25 18:00',NULL,NULL,NULL,'Olympia Stadium, Berlin');
INSERT INTO Matches VALUES (32,1,'D','fr','pl','2024-06-25 18:00',NULL,NULL,NULL,'Signal Iduna Park, Dortmund');

--- Group E
INSERT INTO Countries VALUES ('be','Belgium','E');
INSERT INTO Countries VALUES ('ua','Ukraine','E');
INSERT INTO Countries VALUES ('ro','Romania','E');
INSERT INTO Countries VALUES ('sk','Slovakia','E');

INSERT INTO Matches VALUES ( 9,1,'E','be','sk','2024-06-17 18:00',NULL,NULL,NULL,'Deutsche Bank Park, Frankfurt am Main');
INSERT INTO Matches VALUES (10,1,'E','ro','ua','2024-06-17 15:00',NULL,NULL,NULL,'Allianz Arena, Munich');
INSERT INTO Matches VALUES (21,1,'E','sk','ua','2024-06-21 15:00',NULL,NULL,NULL,'Merkur Spiel-Arena, Düsseldorf');
INSERT INTO Matches VALUES (22,1,'E','be','ro','2024-06-22 21:00',NULL,NULL,NULL,'RheinEnergieStadium, Cologne');
INSERT INTO Matches VALUES (33,1,'E','sk','ro','2024-06-26 18:00',NULL,NULL,NULL,'Deutsche Bank Park, Frankfurt am Main');
INSERT INTO Matches VALUES (34,1,'E','ua','be','2024-06-26 18:00',NULL,NULL,NULL,'MHPArena, Stuttgart');

--- Group F
INSERT INTO Countries VALUES ('tr','Turkye','F');
INSERT INTO Countries VALUES ('pt','Portugal','F');
INSERT INTO Countries VALUES ('ge','Georgia','F');
INSERT INTO Countries VALUES ('cz','Czechia','F');

INSERT INTO Matches VALUES (11,1,'F','tr','ge','2024-06-18 18:00',NULL,NULL,NULL,'Signal Iduna Park, Dortmund');
INSERT INTO Matches VALUES (12,1,'F','pt','cz','2024-06-18 21:00',NULL,NULL,NULL,'Red Bull Arena, Leipzig');
INSERT INTO Matches VALUES (23,1,'F','ge','cz','2024-06-22 15:00',NULL,NULL,NULL,'Volkspark Stadium, Hamburg');
INSERT INTO Matches VALUES (24,1,'F','tr','pt','2024-06-22 18:00',NULL,NULL,NULL,'Signal Iduna Park, Dortmund');
INSERT INTO Matches VALUES (35,1,'F','ge','pt','2024-06-26 21:00',NULL,NULL,NULL,'Veltins-Arena, Gelsenkirchen');
INSERT INTO Matches VALUES (36,1,'F','cz','tr','2024-06-26 21:00',NULL,NULL,NULL,'Volkspark Stadium, Hamburg');

-----------------------------------------------------------------------------
-- The matches played in the knockout stages                               --
-----------------------------------------------------------------------------

--- Knockout - Round of 16
INSERT INTO Matches VALUES (37,2,NULL,NULL,NULL,'2024-06-29 18:00',NULL,'2A','2B','Olympia Stadium, Berlin');
INSERT INTO Matches VALUES (38,2,NULL,NULL,NULL,'2024-06-29 21:00',NULL,'1A','2C','Signal Iduna Park, Dortmund');
INSERT INTO Matches VALUES (39,2,NULL,NULL,NULL,'2024-06-30 21:00',NULL,'1B','3A/D/E/F','RheinEnergieStadium, Cologne');
INSERT INTO Matches VALUES (40,2,NULL,NULL,NULL,'2024-06-30 18:00',NULL,'1C','3D/E/F','Veltins-Arena, Gelsenkirchen');
INSERT INTO Matches VALUES (41,2,NULL,NULL,NULL,'2024-07-01 21:00',NULL,'1F','3A/B/C','Deutsche Bank Park, Frankfurt am Main');
INSERT INTO Matches VALUES (42,2,NULL,NULL,NULL,'2024-07-01 18:00',NULL,'2D','2E','Merkur Spiel-Arena, Düsseldorf');
INSERT INTO Matches VALUES (43,2,NULL,NULL,NULL,'2024-07-02 18:00',NULL,'1E','3A/B/C/D','Allianz Arena, Munich');
INSERT INTO Matches VALUES (44,2,NULL,NULL,NULL,'2024-07-02 21:00',NULL,'1D','2F','Red Bull Arena, Leipzig');

--- Knockout - Quarter-finals
INSERT INTO Matches VALUES (45,3,NULL,NULL,NULL,'2024-07-05 18:00',NULL,'W39','W37','MHPArena, Stuttgart');
INSERT INTO Matches VALUES (46,3,NULL,NULL,NULL,'2024-07-06 21:00',NULL,'W41','W42','Volkspark Stadium, Hamburg');
INSERT INTO Matches VALUES (47,3,NULL,NULL,NULL,'2024-07-06 21:00',NULL,'W43','W44','Merkur Spiel-Arena, Düsseldorf');
INSERT INTO Matches VALUES (48,3,NULL,NULL,NULL,'2024-07-06 18:00',NULL,'W40','W38','Olympia Stadium, Berlin');

--- Knockout - Semi-finals
INSERT INTO Matches VALUES (49,4,NULL,NULL,NULL,'2024-07-09 21:00',NULL,'W45','W46','Allianz Arena, Munich');
INSERT INTO Matches VALUES (50,4,NULL,NULL,NULL,'2024-07-10 21:00',NULL,'W47','W48','Signal Iduna Park, Dortmund');

--- Knockout - Final
INSERT INTO Matches VALUES (51,5,NULL,NULL,NULL,'2024-07-14 21:00',NULL,'W49','W50','Olympia Stadium, Berlin');

--- Champion
INSERT INTO Matches VALUES (52,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
