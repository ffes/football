-----------------------------------------------------------------------------
-- Europees Kampioenschap Voetbal 2024 in Duitsland                        --
-----------------------------------------------------------------------------
-- De fases die het toernooi heeft                                         --
-----------------------------------------------------------------------------

INSERT INTO Stages VALUES (1,'Groepsfase',0,36,1,1);
INSERT INTO Stages VALUES (2,'Achtste finales',1,16,2,2);
INSERT INTO Stages VALUES (3,'Kwartfinales',1,8,4,3);
INSERT INTO Stages VALUES (4,'Halve finales',1,4,7,4);
INSERT INTO Stages VALUES (5,'Finale',1,2,10,5);
INSERT INTO Stages VALUES (6,'Kampioen',1,1,20,6);

-----------------------------------------------------------------------------
-- De landen die deelnemen aan het toernooi                                --
-- En de wedstrijden die gespeeld worden in de groepsfase                  --
-----------------------------------------------------------------------------

--- Groep A
INSERT INTO Countries VALUES ('ch','Zwitserland','A');
INSERT INTO Countries VALUES ('de','Duitsland','A');
INSERT INTO Countries VALUES ('hu','Hongarije','A');
INSERT INTO Countries VALUES ('sc','Schotland','A');

INSERT INTO Matches VALUES ( 1,1,'A','de','sc','2024-06-14 21:00',NULL,NULL,NULL,'Allianz Arena, München');
INSERT INTO Matches VALUES ( 2,1,'A','hu','ch','2024-06-15 15:00',NULL,NULL,NULL,'RheinEnergieStadion, Keulen');
INSERT INTO Matches VALUES (13,1,'A','sc','ch','2024-06-19 21:00',NULL,NULL,NULL,'RheinEnergieStadion, Keulen');
INSERT INTO Matches VALUES (14,1,'A','de','hu','2024-06-19 18:00',NULL,NULL,NULL,'MHPArena, Stuttgart');
INSERT INTO Matches VALUES (25,1,'A','ch','de','2024-06-23 21:00',NULL,NULL,NULL,'Deutsche Bank Park, Frankfurt am Main');
INSERT INTO Matches VALUES (26,1,'A','sc','hu','2024-06-23 21:00',NULL,NULL,NULL,'MHPArena, Stuttgart');

--- Groep B
INSERT INTO Countries VALUES ('al','Albanië','B');
INSERT INTO Countries VALUES ('es','Spanje','B');
INSERT INTO Countries VALUES ('hr','Kroatië','B');
INSERT INTO Countries VALUES ('it','Italië','B');

INSERT INTO Matches VALUES ( 3,1,'B','es','hr','2024-06-15 18:00',NULL,NULL,NULL,'Olympiastadion, Berlijn');
INSERT INTO Matches VALUES ( 4,1,'B','it','al','2024-06-15 21:00',NULL,NULL,NULL,'Signal Iduna Park, Dortmund');
INSERT INTO Matches VALUES (15,1,'B','hr','al','2024-06-19 15:00',NULL,NULL,NULL,'Volksparkstadion, Hamburg');
INSERT INTO Matches VALUES (16,1,'B','es','it','2024-06-20 21:00',NULL,NULL,NULL,'Veltins-Arena, Gelsenkirchen');
INSERT INTO Matches VALUES (27,1,'B','al','es','2024-06-24 21:00',NULL,NULL,NULL,'Merkur Spiel-Arena, Düsseldorf');
INSERT INTO Matches VALUES (28,1,'B','hr','it','2024-06-24 21:00',NULL,NULL,NULL,'Red Bull Arena, Leipzig');

--- Groep C
INSERT INTO Countries VALUES ('dk','Denemarken','C');
INSERT INTO Countries VALUES ('en','Engeland','C');
INSERT INTO Countries VALUES ('rs','Servië','C');
INSERT INTO Countries VALUES ('si','Slovenië','C');

INSERT INTO Matches VALUES ( 5,1,'C','rs','en','2024-06-16 21:00',NULL,NULL,NULL,'Veltins-Arena, Gelsenkirchen');
INSERT INTO Matches VALUES ( 6,1,'C','si','dk','2024-06-16 18:00',NULL,NULL,NULL,'MHPArena, Stuttgart');
INSERT INTO Matches VALUES (17,1,'C','dk','en','2024-06-20 18:00',NULL,NULL,NULL,'Deutsche Bank Park, Frankfurt am Main');
INSERT INTO Matches VALUES (18,1,'C','si','rs','2024-06-20 15:00',NULL,NULL,NULL,'Allianz Arena, München');
INSERT INTO Matches VALUES (29,1,'C','en','si','2024-06-25 21:00',NULL,NULL,NULL,'RheinEnergieStadion, Keulen');
INSERT INTO Matches VALUES (30,1,'C','dk','rs','2024-06-25 21:00',NULL,NULL,NULL,'Allianz Arena, München');

--- Groep D
INSERT INTO Countries VALUES ('at','Oostenrijk','D');
INSERT INTO Countries VALUES ('fr','Frankrijk','D');
INSERT INTO Countries VALUES ('nl','Nederland','D');
INSERT INTO Countries VALUES ('pl','Polen','D');

INSERT INTO Matches VALUES ( 7,1,'D','pl','nl','2024-06-16 15:00',NULL,NULL,NULL,'Volksparkstadion, Hamburg');
INSERT INTO Matches VALUES ( 8,1,'D','at','fr','2024-06-17 21:00',NULL,NULL,NULL,'Merkur Spiel-Arena, Düsseldorf');
INSERT INTO Matches VALUES (19,1,'D','pl','at','2024-06-21 18:00',NULL,NULL,NULL,'Olympiastadion, Berlijn');
INSERT INTO Matches VALUES (20,1,'D','nl','fr','2024-06-21 21:00',NULL,NULL,NULL,'Red Bull Arena, Leipzig');
INSERT INTO Matches VALUES (31,1,'D','nl','at','2024-06-25 18:00',NULL,NULL,NULL,'Olympiastadion, Berlijn');
INSERT INTO Matches VALUES (32,1,'D','fr','pl','2024-06-25 18:00',NULL,NULL,NULL,'Signal Iduna Park, Dortmund');

--- Groep E
INSERT INTO Countries VALUES ('be','België','E');
INSERT INTO Countries VALUES ('ua','Oekraïne','E');
INSERT INTO Countries VALUES ('ro','Roemenië','E');
INSERT INTO Countries VALUES ('sk','Slowakije','E');

INSERT INTO Matches VALUES ( 9,1,'E','be','sk','2024-06-17 18:00',NULL,NULL,NULL,'Deutsche Bank Park, Frankfurt am Main');
INSERT INTO Matches VALUES (10,1,'E','ro','ua','2024-06-17 15:00',NULL,NULL,NULL,'Allianz Arena, München');
INSERT INTO Matches VALUES (21,1,'E','sk','ua','2024-06-21 15:00',NULL,NULL,NULL,'Merkur Spiel-Arena, Düsseldorf');
INSERT INTO Matches VALUES (22,1,'E','be','ro','2024-06-22 21:00',NULL,NULL,NULL,'RheinEnergieStadion, Keulen');
INSERT INTO Matches VALUES (33,1,'E','sk','ro','2024-06-26 18:00',NULL,NULL,NULL,'Deutsche Bank Park, Frankfurt am Main');
INSERT INTO Matches VALUES (34,1,'E','ua','be','2024-06-26 18:00',NULL,NULL,NULL,'MHPArena, Stuttgart');

--- Groep F
INSERT INTO Countries VALUES ('tr','Turkije','F');
INSERT INTO Countries VALUES ('pt','Portugal','F');
INSERT INTO Countries VALUES ('ge','Georgië','F');
INSERT INTO Countries VALUES ('cz','Tsjechië','F');

INSERT INTO Matches VALUES (11,1,'F','tr','ge','2024-06-18 18:00',NULL,NULL,NULL,'Signal Iduna Park, Dortmund');
INSERT INTO Matches VALUES (12,1,'F','pt','cz','2024-06-18 21:00',NULL,NULL,NULL,'Red Bull Arena, Leipzig');
INSERT INTO Matches VALUES (23,1,'F','ge','cz','2024-06-22 15:00',NULL,NULL,NULL,'Volksparkstadion, Hamburg');
INSERT INTO Matches VALUES (24,1,'F','tr','pt','2024-06-22 18:00',NULL,NULL,NULL,'Signal Iduna Park, Dortmund');
INSERT INTO Matches VALUES (35,1,'F','ge','pt','2024-06-26 21:00',NULL,NULL,NULL,'Veltins-Arena, Gelsenkirchen');
INSERT INTO Matches VALUES (36,1,'F','cz','tr','2024-06-26 21:00',NULL,NULL,NULL,'Volksparkstadion, Hamburg');

-----------------------------------------------------------------------------
-- De wedstrijden die gespeeld worden in de knockoutfase                   --
-----------------------------------------------------------------------------

--- Achtste finales
INSERT INTO Matches VALUES (37,2,NULL,NULL,NULL,'2024-06-29 18:00',NULL,'2A','2B','Olympiastadion, Berlijn');
INSERT INTO Matches VALUES (38,2,NULL,NULL,NULL,'2024-06-29 21:00',NULL,'1A','2C','Signal Iduna Park, Dortmund');
INSERT INTO Matches VALUES (39,2,NULL,NULL,NULL,'2024-06-30 21:00',NULL,'1B','3A/D/E/F','RheinEnergieStadion, Keulen');
INSERT INTO Matches VALUES (40,2,NULL,NULL,NULL,'2024-06-30 18:00',NULL,'1C','3D/E/F','Veltins-Arena, Gelsenkirchen');
INSERT INTO Matches VALUES (41,2,NULL,NULL,NULL,'2024-07-01 21:00',NULL,'1F','3A/B/C','Deutsche Bank Park, Frankfurt am Main');
INSERT INTO Matches VALUES (42,2,NULL,NULL,NULL,'2024-07-01 18:00',NULL,'2D','2E','Merkur Spiel-Arena, Düsseldorf');
INSERT INTO Matches VALUES (43,2,NULL,NULL,NULL,'2024-07-02 18:00',NULL,'1E','3A/B/C/D','Allianz Arena, München');
INSERT INTO Matches VALUES (44,2,NULL,NULL,NULL,'2024-07-02 21:00',NULL,'1D','2F','Red Bull Arena, Leipzig');

--- Kwartfinales
INSERT INTO Matches VALUES (45,3,NULL,NULL,NULL,'2024-07-05 18:00',NULL,'W39','W37','MHPArena, Stuttgart');
INSERT INTO Matches VALUES (46,3,NULL,NULL,NULL,'2024-07-06 21:00',NULL,'W41','W42','Volksparkstadion, Hamburg');
INSERT INTO Matches VALUES (47,3,NULL,NULL,NULL,'2024-07-06 21:00',NULL,'W43','W44','Merkur Spiel-Arena, Düsseldorf');
INSERT INTO Matches VALUES (48,3,NULL,NULL,NULL,'2024-07-06 18:00',NULL,'W40','W38','Olympiastadion, Berlijn');

--- Halve finales
INSERT INTO Matches VALUES (49,4,NULL,NULL,NULL,'2024-07-09 21:00',NULL,'W45','W46','Allianz Arena, München');
INSERT INTO Matches VALUES (50,4,NULL,NULL,NULL,'2024-07-10 21:00',NULL,'W47','W48','Signal Iduna Park, Dortmund');

--- Finale
INSERT INTO Matches VALUES (51,5,NULL,NULL,NULL,'2024-07-14 21:00',NULL,'W49','W50','Olympiastadion, Berlijn');

--- Kampioen
INSERT INTO Matches VALUES (52,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
