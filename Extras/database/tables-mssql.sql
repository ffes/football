-----------------------------------------------------------------------------
-- Table definitions for SQL Server for the Football Predictions Website   --
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- First drop all the tables in the right order                            --
-----------------------------------------------------------------------------
DROP TABLE IF EXISTS Answers;
DROP TABLE IF EXISTS Users;
DROP TABLE IF EXISTS Matches;
DROP TABLE IF EXISTS Stages;
DROP TABLE IF EXISTS Countries;
GO

-----------------------------------------------------------------------------
-- The countries that takes part in the tournament, including the group    --
-- they play their first round in. The CountryID is the ISO 3166-1 code    --
-- where possible. For the countries in the UK a custom code is used.      --
-----------------------------------------------------------------------------

CREATE TABLE Countries (
	CountryID		NVARCHAR(2)		NOT NULL	 PRIMARY KEY,
	CountryName		NVARCHAR(50)	NOT NULL,
	GroupID			NVARCHAR(1)		NOT NULL,
);
GO

-----------------------------------------------------------------------------
-- The stages the tournament has.                                          --
-- It also contains the number of answers in the stage to be able to       --
-- check if a player has filled in all the answers and the numbers of      --
-- points you will get for a correct prediction in that stage.             --
-----------------------------------------------------------------------------

CREATE TABLE Stages (
	StageID			INT				NOT NULL	PRIMARY KEY,
	StageName		NVARCHAR(50)	NOT NULL,
	Knockout		BIT				NOT NULL	DEFAULT 0,
	Answers			INT				NOT NULL,
	Points			INT				NOT NULL,
	OrderBy			INT				NOT NULL
);
GO

-----------------------------------------------------------------------------
-- The matches played in the tournament                                    --
-----------------------------------------------------------------------------

CREATE TABLE Matches (
	MatchID			INT				NOT NULL	 PRIMARY KEY,
	StageID			INT				NOT NULL,
	GroupID			NVARCHAR(1),
	HomeID			NVARCHAR(2),
	AwayID			NVARCHAR(2),
	MatchDate		DATETIME,
	MatchResult		INT,
	HomeFrom		NVARCHAR(10),
	AwayFrom		NVARCHAR(10),
	Stadium			NVARCHAR(255),
	FOREIGN KEY(StageID) REFERENCES Stages(StageID),
	FOREIGN KEY(HomeID) REFERENCES Countries(CountryID),
	FOREIGN KEY(AwayID) REFERENCES Countries(CountryID)
);
CREATE INDEX Matches_StageID ON Matches(StageID);
GO

-----------------------------------------------------------------------------
-- The users participating in our game                                     --
-----------------------------------------------------------------------------

CREATE TABLE Users (
	UserID		INT IDENTITY(1,1)	NOT NULL	PRIMARY KEY,
	EMail		NVARCHAR(50)		NOT NULL,
	Password	NVARCHAR(255)		NOT NULL,
	RealName	NVARCHAR(50)		NOT NULL,
	NickName	NVARCHAR(50)		NOT NULL,
	SigninKey	NVARCHAR(255)		NOT NULL,
	Enabled		BIT					NOT NULL	DEFAULT 0,
	UserLevel	INT					NOT NULL	DEFAULT 0,
);
CREATE UNIQUE INDEX Users_EMail ON Users(EMail);
GO

-----------------------------------------------------------------------------
-- The answers given by the users                                          --
-----------------------------------------------------------------------------

CREATE TABLE Answers (
	AnswerID	INT IDENTITY(1,1)	NOT NULL	PRIMARY KEY,
	UserID		INT					NOT NULL,
	MatchID		INT,
	Answer		INT,
	StageID		INT					NOT NULL,
	CountryID	NVARCHAR(2),
	FormID		INT,
	Points		INT					NOT NULL	DEFAULT 0,
	FOREIGN KEY(UserID) REFERENCES Users(UserID),
	FOREIGN KEY(MatchID) REFERENCES Matches(MatchID),
	FOREIGN KEY(StageID) REFERENCES Stages(StageID),
	FOREIGN KEY(CountryID) REFERENCES Countries(CountryID)
);
GO
