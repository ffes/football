-----------------------------------------------------------------------------
-- Table definitions for the Football Predictions Website                  --
-- This is the SQLite version, used for testing and development            --
-----------------------------------------------------------------------------

PRAGMA foreign_keys = ON;

-----------------------------------------------------------------------------
-- The countries that takes part in the tournament, including the group    --
-- they play their first round in                                          --
-----------------------------------------------------------------------------

DROP TABLE IF EXISTS Countries;
CREATE TABLE Countries (
	CountryID		VARCHAR(2)		NOT NULL	PRIMARY KEY,
	CountryName		VARCHAR(50)		NOT NULL,
	GroupID			VARCHAR(1)		NOT NULL
);

-----------------------------------------------------------------------------
-- The stages the tournament has                                           --
-- It also contains the number of answers in the stage to be able to       --
-- check if a player has filled in all the answers and the numbers of      --
-- point you'll get for a correct prediction in that stage                 --
-----------------------------------------------------------------------------

DROP TABLE IF EXISTS Stages;
CREATE TABLE Stages (
	StageID			INTEGER				NOT NULL	PRIMARY KEY,
	StageName		VARCHAR(50)			NOT NULL,
	Knockout		BIT					NOT NULL	DEFAULT 0,
	Answers			INTEGER				NOT NULL,
	Points			INTEGER				NOT NULL,
	OrderBy			INTEGER				NOT NULL
);

-----------------------------------------------------------------------------
-- The matches played in the tournament                                    --
-----------------------------------------------------------------------------

DROP TABLE IF EXISTS Matches;
CREATE TABLE Matches (
	MatchID			INTEGER				NOT NULL	PRIMARY KEY,
	StageID			INTEGER				NOT NULL,
	GroupID			VARCHAR(1),
	HomeID			VARCHAR(2),
	AwayID			VARCHAR(2),
	MatchDate		DATETIME,
	MatchResult		INTEGER,
	HomeFrom		VARCHAR(10),
	AwayFrom		VARCHAR(10),
	Stadium			VARCHAR(50),
	FOREIGN KEY(StageID) REFERENCES Stages(StageID),
	FOREIGN KEY(HomeID) REFERENCES Countries(CountryID) ON UPDATE CASCADE,
	FOREIGN KEY(AwayID) REFERENCES Countries(CountryID) ON UPDATE CASCADE
);
CREATE INDEX Matches_StageID ON Matches(StageID);

-----------------------------------------------------------------------------
-- The users participating in our game                                     --
-----------------------------------------------------------------------------

DROP TABLE IF EXISTS Users;
CREATE TABLE Users (
	UserID		INTEGER					NOT NULL	PRIMARY KEY,	-- Automatically becomes AUTOINCREMENT
	EMail		VARCHAR(50)				NOT NULL,
	Password	VARCHAR(100)			NOT NULL,
	RealName	VARCHAR(50)				NOT NULL,
	NickName	VARCHAR(50)				NOT NULL,
	SigninKey	VARCHAR(50)				NOT NULL,
	Enabled		BIT						NOT NULL	DEFAULT 0,
	UserLevel	INTEGER					NOT NULL	DEFAULT 0
);
CREATE UNIQUE INDEX Users_EMail ON Users(EMail);

-----------------------------------------------------------------------------
-- The answers given by the users                                          --
-----------------------------------------------------------------------------

DROP TABLE IF EXISTS Answers;
CREATE TABLE Answers (
	AnswerID	INTEGER					NOT NULL	PRIMARY KEY,	-- Automatically becomes AUTOINCREMENT
	UserID		INTEGER					NOT NULL,
	MatchID		INTEGER,
	Answer		INTEGER,
	StageID		INTEGER					NOT NULL,
	CountryID	VARCHAR(2)							DEFAULT NULL,
	FormID		INTEGER,
	Points		INTEGER					NOT NULL	DEFAULT 0,
	FOREIGN KEY(UserID) REFERENCES Users(UserID),
	FOREIGN KEY(MatchID) REFERENCES Matches(MatchID),
	FOREIGN KEY(StageID) REFERENCES Stages(StageID),
	FOREIGN KEY(CountryID) REFERENCES Countries(CountryID)
);
