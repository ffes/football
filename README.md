Football predictions
====================

This is a project to maintain a web-based football predictions game for major tournaments in football (aka soccer).

It will be written in ASP.NET Core and Angular and is still in very early stages of development.
See [my blog](https://www.fesevur.com/blog/) for a series of posts how this is being developed.
