using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Football.Helpers;
using Football.Models;
using Microsoft.Extensions.Logging;

namespace Football.Repositories;

public class UsersRepository(
	ILogger<UsersRepository> logger,
	IDatabaseHelper dbhelper
	): IUsersRepository
{
	public async Task<IEnumerable<UserModel>> GetUsers()
	{
		var sql =
			@"SELECT Users.UserID,
				Users.EMail, Users.RealName, Users.NickName,
				Users.Enabled, Users.UserLevel,
				COUNT(Answers.AnswerID) AS Answers
			FROM Users
				LEFT JOIN Answers ON Users.UserID = Answers.UserID
			GROUP BY Users.UserID,
				Users.EMail, Users.RealName, Users.NickName,
				Users.Enabled, Users.UserLevel
			ORDER BY Users.UserID";

		using var cnn = dbhelper.GetDatabaseConnection();
		var users = await cnn.QueryAsync<UserModel>(sql);
		return users;
	}

	public async Task<UserModel> GetUserById(int id)
	{
		var sql =
			@"SELECT *
			FROM Users
			WHERE UserID = @id";

		using var cnn = dbhelper.GetDatabaseConnection();
		var user = await cnn.QueryFirstOrDefaultAsync<UserModel>(sql, new { id });
		return user;
	}

	public async Task<UserModel> GetUserByEmail(string email)
	{
		string sql =
			@"SELECT *
			FROM Users
			WHERE EMail = @email";

		using var cnn = dbhelper.GetDatabaseConnection();
		var user = await cnn.QueryFirstOrDefaultAsync<UserModel>(sql, new { email });
		return user;
	}

	public async Task<UserModel> GetUserBySignInKey(string signinKey)
	{
		string sql =
			@"SELECT *
			FROM Users
			WHERE SigninKey = @signinkey";

		using var cnn = dbhelper.GetDatabaseConnection();
		var user = await cnn.QueryFirstOrDefaultAsync<UserModel>(sql, new { signinKey });
		return user;
	}

	public async Task<bool> EnableUser(int userid, bool enabled)
	{
		string sql =
			@"UPDATE Users
			SET Enabled = @enabled
			WHERE UserID = @userid";

		using var cnn = dbhelper.GetDatabaseConnection();
		int rows = await cnn.ExecuteAsync(sql, new { userid, enabled });
		return rows > 0;
	}

	public async Task<bool> SaveSignup(SignUpModel signUp)
	{
		string sql =
			@"INSERT INTO Users (
				EMail, Password,
				RealName, NickName, SigninKey
			) VALUES (
				@EMail, @Password,
				@RealName, @NickName, @ActivationKey
			)";

		using var cnn = dbhelper.GetDatabaseConnection();
		int rows = await cnn.ExecuteAsync(sql, signUp);
		return rows > 0;
	}
}
