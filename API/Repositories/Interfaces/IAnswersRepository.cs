using System.Collections.Generic;
using System.Threading.Tasks;
using Football.Models;

namespace Football.Repositories;

public interface IAnswersRepository
{
	public Task<bool> AddAnswerForMatch(long userid, long matchid, int prediction);
	public Task<bool> AddAnswerForStage(long userid, int stageid, string countryid, string formid);
	public Task<bool> DeleteAnswerForMatch(long userid, long matchid);
	public Task<bool> DeleteAnswerForStage(long userid, long stageid, string formid);
	public Task<IEnumerable<AnswersInGroupStage>> GetNumberOfAnswersPerUserInGroupStage(long userid);
	public Task<bool> IsCountryInStage(long userid, int stageid, string countryid);
	public Task<IEnumerable<UserModel>> GetStandings();
	public Task ResetPoint();
	public Task SetPointsForGroupStage();
	public Task SetPointsForKoStage();
	public Task<IEnumerable<MatchModel>> GetUserMatches(int userid);
	public Task<IEnumerable<UserKnockoutModel>> GetUserKnockout(int userid);
}
