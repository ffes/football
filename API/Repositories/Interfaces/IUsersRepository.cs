using System.Collections.Generic;
using System.Threading.Tasks;
using Football.Models;

namespace Football.Repositories;

public interface IUsersRepository
{
	public Task<UserModel> GetUserByEmail(string email);
	public Task<UserModel> GetUserBySignInKey(string signinKey);
	public Task<bool> EnableUser(int userId, bool enabled);
	public Task<bool> SaveSignup(SignUpModel signUp);
	public Task<UserModel> GetUserById(int id);
	public Task<IEnumerable<UserModel>> GetUsers();
}
