using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Football.Models;

namespace Football.Repositories;

public interface IMatchesRepository
{
	public Task<IEnumerable<MatchModel>> GetMatchesInGroup(long userid, string groupid);
	public Task<IEnumerable<MatchModel>> GetMatchesInGroups();
	public Task<DateTime> GetFirstMatchDate();
	public Task<DateTime> GetFirstKoMatchDate();
	public Task<MatchModel> GetMatchById(int matchid);
	public Task<IEnumerable<StageModel>> GetKnockoutStages(long userid);
	public Task<IEnumerable<StageMatchModel>> GetKnockoutStage(int stageid);
	public Task<IEnumerable<StageMatchModel>> GetKnockoutStage(long userid, int stageid);
	public Task<bool> SetMatchResult(long matchid, int result);
	public Task<IEnumerable<MatchModel>> GetKnockoutStageMatches();
	public Task<bool> SetMatchHome(int matchid, string countryid);
	public Task<bool> SetMatchAway(int matchid, string countryid);
}
