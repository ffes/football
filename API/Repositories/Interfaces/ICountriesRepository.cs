using System.Collections.Generic;
using System.Threading.Tasks;
using Football.Models;

namespace Football.Repositories;

public interface ICountriesRepository
{
	public Task<IEnumerable<CountryModel>> GetCountries();
	public Task<IEnumerable<CountryModel>> GetCountriesInGroups(string[] groups);
	public Task<IEnumerable<CountryModel>> GetCountriesInStage(long userid, int stageid);
}
