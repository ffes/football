using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Football.Helpers;
using Football.Models;
using Microsoft.Extensions.Logging;

namespace Football.Repositories;

public class MatchesRepository(ILogger<MatchesRepository> logger, IDatabaseHelper dbhelper): IMatchesRepository
{
	public async Task<IEnumerable<MatchModel>> GetMatchesInGroup(long userid, string groupid)
	{
		// Retrieve all the matches in a group
		string sql =
			@"SELECT Matches.MatchID, Matches.GroupID,
				Matches.StageID, Stages.StageName,
				Matches.HomeID, HomeCountry.CountryName AS HomeName,
				Matches.AwayID, AwayCountry.CountryName AS AwayName,
				Matches.MatchDate, Matches.Stadium,
				CASE WHEN A.Answer IS NULL THEN 0 ELSE A.Answer END AS Answer
			FROM Matches
				INNER JOIN Countries AS HomeCountry ON Matches.HomeID = HomeCountry.CountryID
				INNER JOIN Countries AS AwayCountry ON Matches.AwayID = AwayCountry.CountryID
				INNER JOIN Stages ON Matches.StageID = Stages.StageID
				LEFT JOIN (
					SELECT *
					FROM Answers
					WHERE UserID = @userid
				) AS A ON Matches.MatchID = A.MatchID
			WHERE Matches.StageID = 1
			AND Matches.GroupID = @groupid
			ORDER BY Matches.MatchDate, Matches.MatchID";

		using var cnn = dbhelper.GetDatabaseConnection();
		var matches = await cnn.QueryAsync<MatchModel>(sql, new { userid, groupid });

		// In the database all the times are stored as local time, so set the kind right.
		foreach (var match in matches)
		{
			match.MatchDate = DateTime.SpecifyKind(match.MatchDate, DateTimeKind.Local);
		}

		return matches;
	}

	public async Task<IEnumerable<MatchModel>> GetMatchesInGroups()
	{
		// Retrieve all the matches in the group stage
		string sql =
			@"SELECT Matches.*, Stages.StageName,
					HomeCountry.CountryName AS HomeName, AwayCountry.CountryName AS AwayName
			FROM Matches
				INNER JOIN Stages ON Matches.StageID = Stages.StageID
				INNER JOIN Countries As HomeCountry ON Matches.HomeID = HomeCountry.CountryID
				INNER JOIN Countries As AwayCountry ON Matches.AwayID = AwayCountry.CountryID
			WHERE Matches.StageID = 1
			ORDER BY Matches.MatchDate, Matches.MatchID";

		using var cnn = dbhelper.GetDatabaseConnection();
		var matches = await cnn.QueryAsync<MatchModel>(sql);

		// In the database all the times are stored as local time, so set the kind right.
		foreach (var match in matches)
		{
			match.MatchDate = DateTime.SpecifyKind(match.MatchDate, DateTimeKind.Local);
		}

		return matches;
	}

	public async Task<DateTime> GetFirstMatchDate()
	{
		using var cnn = dbhelper.GetDatabaseConnection();
		var firstmatch = await cnn.QueryFirstOrDefaultAsync<DateTime>("SELECT MIN(MatchDate) FROM Matches");

		// In the database all the times are stored as local time, so set the kind right.
		return DateTime.SpecifyKind(firstmatch, DateTimeKind.Local);
	}

	public async Task<DateTime> GetFirstKoMatchDate()
	{
		string sql =
			@"SELECT MIN(MatchDate)
			FROM Matches
			WHERE StageID > 1";

		using var cnn = dbhelper.GetDatabaseConnection();
		var firstmatch = await cnn.QueryFirstOrDefaultAsync<DateTime>(sql);

		// In the database all the times are stored as local time, so set the kind right.
		return DateTime.SpecifyKind(firstmatch, DateTimeKind.Local);
	}

	public async Task<MatchModel> GetMatchById(int matchid)
	{
		string sql =
			@"SELECT Matches.*, HomeCountries.CountryName AS HomeName, AwayCountries.CountryName AS AwayName
			FROM Matches
				LEFT JOIN Countries AS HomeCountries ON Matches.HomeID = HomeCountries.CountryID
				LEFT JOIN Countries AS AwayCountries ON Matches.AwayiD = AwayCountries.CountryID
			WHERE Matches.MatchID = @matchid";

		using var cnn = dbhelper.GetDatabaseConnection();
		var match = await cnn.QueryFirstOrDefaultAsync<MatchModel>(sql, new { matchid });

		// In the database all the times are stored as local time, so set the kind right.
		match.MatchDate = DateTime.SpecifyKind(match.MatchDate, DateTimeKind.Local);

		return match;
	}

	public async Task<IEnumerable<StageModel>> GetKnockoutStages(long userid)
	{
		string sql =
			@"SELECT Stages.*, (Answ.Cnt = Stages.Answers) AS Completed
			FROM Stages
				LEFT JOIN (
					SELECT StageID, COUNT(Answers.AnswerID) AS Cnt
					FROM Answers
					WHERE UserID = @userid
					GROUP BY StageID
				) AS Answ ON Stages.StageID = Answ.StageID
			WHERE Stages.Knockout = 1
			ORDER BY Stages.OrderBy";

		using var cnn = dbhelper.GetDatabaseConnection();
		var matches = await cnn.QueryAsync<StageModel>(sql, new { userid });
		return matches;
	}

	public async Task<IEnumerable<StageMatchModel>> GetKnockoutStage(long userid, int stageid)
	{
		string sql =
			@"SELECT Answers.*, Countries.CountryName, Stages.StageName
			FROM Answers
				INNER JOIN Countries ON Answers.CountryID = Countries.CountryID
				INNER JOIN Stages ON Answers.StageID = Stages.StageID
			WHERE Answers.StageID = @stageid
			AND Answers.UserID = @userid
			ORDER BY Answers.FormID";

		using var cnn = dbhelper.GetDatabaseConnection();
		var matches = await cnn.QueryAsync<StageMatchModel>(sql, new { userid, stageid });
		return matches;
	}

	public async Task<IEnumerable<StageMatchModel>> GetKnockoutStage(int stageid)
	{
		string sql =
			@"SELECT Matches.*, Stages.StageName, Stages.Answers
			FROM Matches
				INNER JOIN Stages ON Matches.StageID = Stages.StageID
			WHERE Matches.StageID = @stageid
			ORDER BY Matches.MatchDate, Matches.MatchID";

		using var cnn = dbhelper.GetDatabaseConnection();
		var matches = await cnn.QueryAsync<StageMatchModel>(sql, new { stageid });
		return matches;
	}

	public async Task<IEnumerable<MatchModel>> GetKnockoutStageMatches()
	{
		string sql =
			@"SELECT Matches.*, Stages.StageName
			FROM Matches
				INNER JOIN Stages ON Matches.StageID = Stages.StageID
			WHERE Matches.StageID > 1
			ORDER BY StageID, MatchDate, MatchID";

		using var cnn = dbhelper.GetDatabaseConnection();
		var matches = await cnn.QueryAsync<MatchModel>(sql);

		// In the database all the times are stored as local time, so set the kind right.
		foreach (var match in matches)
		{
			match.MatchDate = DateTime.SpecifyKind(match.MatchDate, DateTimeKind.Local);
		}

		return matches;
	}

	public async Task<bool> SetMatchResult(long matchid, int result)
	{
		string sql =
			@"UPDATE Matches
			SET MatchResult = @result
			WHERE MatchID = @matchid";

		using var cnn = dbhelper.GetDatabaseConnection();
		int rows = await cnn.ExecuteAsync(sql, new { matchid, result });
		return rows == 1;
	}

	public async Task<bool> SetMatchHome(int matchid, string countryid)
	{
		string sql =
			@"UPDATE Matches
			SET HomeID = @countryid
			WHERE MatchID = @matchid";

		using var cnn = dbhelper.GetDatabaseConnection();
		int rows = await cnn.ExecuteAsync(sql, new { matchid, countryid });
		return rows == 1;
	}

	public async Task<bool> SetMatchAway(int matchid, string countryid)
	{
		string sql =
			@"UPDATE Matches
			SET AwayID = @countryid
			WHERE MatchID = @matchid";

		using var cnn = dbhelper.GetDatabaseConnection();
		int rows = await cnn.ExecuteAsync(sql, new { matchid, countryid });
		return rows == 1;
	}
}
