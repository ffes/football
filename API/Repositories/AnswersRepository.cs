using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Football.Helpers;
using Football.Models;
using Microsoft.Extensions.Logging;

namespace Football.Repositories;

public class AnswersRepository(ILogger<AnswersRepository> logger, IDatabaseHelper dbhelper): IAnswersRepository
{
	public async Task<bool> AddAnswerForMatch(long userid, long matchid, int prediction)
	{
		string sql =
			@"INSERT INTO Answers(
				UserID, MatchID,
				Answer, StageID
			)
			VALUES (
				@userid, @matchid,
				@prediction, 1
			)";

		using var cnn = dbhelper.GetDatabaseConnection();
		int rows = await cnn.ExecuteAsync(sql, new { userid, matchid, prediction });
		return rows == 1;
	}

	public async Task<bool> AddAnswerForStage(long userid, int stageid, string countryid, string formid)
	{
		string sql =
			@"INSERT INTO Answers(
				UserID, StageID,
				CountryID, FormID
			)
			VALUES (
				@userid, @stageid,
				@countryid, @formid
			)";

		using var cnn = dbhelper.GetDatabaseConnection();
		int rows = await cnn.ExecuteAsync(sql, new { userid, stageid, countryid, formid });
		return rows == 1;
	}

	public async Task<bool> DeleteAnswerForMatch(long userid, long matchid)
	{
		string sql =
			@"DELETE FROM Answers
			WHERE UserID = @userid
			AND MatchID = @matchid";

		using var cnn = dbhelper.GetDatabaseConnection();
		int rows = await cnn.ExecuteAsync(sql, new { userid, matchid });
		return rows == 1;
	}

	public async Task<bool> DeleteAnswerForStage(long userid, long stageid, string formid)
	{
		string sql =
			@"DELETE FROM Answers
			WHERE UserID = @userid
			AND StageID = @stageid
			AND FormID = @formid";

		using var cnn = dbhelper.GetDatabaseConnection();
		int rows = await cnn.ExecuteAsync(sql, new { userid, stageid, formid });
		return rows == 1;
	}

	public async Task<IEnumerable<AnswersInGroupStage>> GetNumberOfAnswersPerUserInGroupStage(long userid)
	{
		string sql =
			@"SELECT Matches.GroupID, COUNT(Matches.MatchID) AS PossibleAnswers, COUNT(A.MatchID) AS GivenAnswers
			FROM Matches
				INNER JOIN Stages ON Matches.StageID = Stages.StageID
				LEFT JOIN (
					SELECT *
					FROM Answers
					WHERE UserID = @userid
				) AS A ON Matches.MatchID = A.MatchID
			WHERE Stages.StageID = 1
			GROUP BY Matches.GroupID";

		using var cnn = dbhelper.GetDatabaseConnection();
		var answers = await cnn.QueryAsync<AnswersInGroupStage>(sql, new { userid });
		return answers;
	}

	public async Task<bool> IsCountryInStage(long userid, int stageid, string countryid)
	{
		string sql =
			@"SELECT CountryID
			FROM Answers
			WHERE StageID = @stageid
			AND UserID = @userid
			AND CountryID = @countryid";

		using var cnn = dbhelper.GetDatabaseConnection();
		var country = await cnn.QueryFirstOrDefaultAsync<string>(sql, new { userid, stageid, countryid });
		return !string.IsNullOrWhiteSpace(country);
	}

	public async Task<IEnumerable<UserModel>> GetStandings()
	{
		string sql =
			@"SELECT Users.UserID, Users.NickName, Users.RealName, Users.Enabled, SUM(Answers.Points) AS Score
			FROM Answers
				INNER JOIN Users ON Answers.UserID = Users.UserID
			WHERE Users.Enabled = 1
			GROUP BY Users.UserID, Users.NickName
			ORDER BY SUM(Answers.Points) DESC, Users.NickName";

		using var cnn = dbhelper.GetDatabaseConnection();
		var answers = await cnn.QueryAsync<UserModel>(sql);
		return answers;
	}

	public async Task ResetPoint()
	{
		string sql = "UPDATE Answers SET Points = 0";

		using var cnn = dbhelper.GetDatabaseConnection();
		await cnn.ExecuteAsync(sql);
	}

	public async Task SetPointsForGroupStage()
	{
		string sql =
			@"UPDATE Answers
			SET Points = 1
			WHERE AnswerID IN (
				SELECT Answers.AnswerID
				FROM Matches
					INNER JOIN Answers ON Matches.MatchID = Answers.MatchID
				WHERE Answers.StageID = 1
				AND Matches.MatchResult = Answers.Answer
			)";

		using var cnn = dbhelper.GetDatabaseConnection();
		await cnn.ExecuteAsync(sql);
	}

	public async Task SetPointsForKoStage()
	{
		string sql =
			@"UPDATE Answers
			SET Points = M.Points
			FROM (
				SELECT Matches.StageID, Matches.HomeID AS CountryID, Stages.Points
				FROM Matches
					INNER JOIN Stages ON Matches.StageID = Stages.StageID
				WHERE Matches.StageID > 1
				AND Matches.HomeID IS NOT NULL
				UNION
				SELECT Matches.StageID, Matches.AwayID AS CountryID, Stages.Points
				FROM Matches
					INNER JOIN Stages ON Matches.StageID = Stages.StageID
				WHERE Matches.StageID > 1
				AND Matches.AwayID IS NOT NULL
			) AS M
			WHERE Answers.StageID = M.StageID
			AND Answers.CountryID = M.CountryID";

		using var cnn = dbhelper.GetDatabaseConnection();
		await cnn.ExecuteAsync(sql);
	}

	public async Task<IEnumerable<MatchModel>> GetUserMatches(int userid)
	{
		string sql =
			@"SELECT Matches.*, Answers.Answer, Users.RealName,
				HomeCountries.CountryName AS HomeName, AwayCountries.CountryName AS AwayName
			FROM Matches
				INNER JOIN Countries AS HomeCountries ON Matches.HomeID = HomeCountries.CountryID
				INNER JOIN Countries AS AwayCountries ON Matches.AwayiD = AwayCountries.CountryID
				INNER JOIN Answers ON Matches.MatchID = Answers.MatchID
				INNER JOIN Users ON Answers.UserID = Users.UserID
			WHERE Matches.StageID = 1
			AND Answers.UserID = @userid
			ORDER BY Matches.MatchDate, Matches.MatchID";

		using var cnn = dbhelper.GetDatabaseConnection();
		var matches = await cnn.QueryAsync<MatchModel>(sql, new { userid });

		// In the database all the times are stored as local time, so set the kind right.
		foreach (var match in matches)
		{
			match.MatchDate = DateTime.SpecifyKind(match.MatchDate, DateTimeKind.Local);
		}

		return matches;
	}

	public async Task<IEnumerable<UserKnockoutModel>> GetUserKnockout(int userid)
	{
		string sql =
			@"SELECT Users.RealName, Stages.StageName, Answers.CountryID, Countries.CountryName, Answers.Points
			FROM Answers
				INNER JOIN Countries ON Answers.CountryID = Countries.CountryID
				INNER JOIN Stages ON Answers.StageID = Stages.StageID
				INNER JOIN Users ON Answers.UserID = Users.UserID
			WHERE Answers.StageID > 1
			AND Answers.UserID = @userid
			ORDER BY Answers.StageID, Countries.CountryName";

		using var cnn = dbhelper.GetDatabaseConnection();
		var results = await cnn.QueryAsync<UserKnockoutModel>(sql, new { userid });
		return results;
	}
}
