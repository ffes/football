using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Football.Helpers;
using Football.Models;
using Microsoft.Extensions.Logging;

namespace Football.Repositories;

public class CountriesRepository(ILogger<CountriesRepository> logger, IDatabaseHelper dbhelper): ICountriesRepository
{
	public async Task<IEnumerable<CountryModel>> GetCountries()
	{
		// Retrieve all the countries
		using var cnn = dbhelper.GetDatabaseConnection();
		return await cnn.QueryAsync<CountryModel>("SELECT * FROM Countries ORDER BY GroupID, CountryName");
	}

	public async Task<IEnumerable<CountryModel>> GetCountriesInGroups(string[] groups)
	{
		string sql =
			@"SELECT *
			FROM Countries
			WHERE GroupID IN @groups
			ORDER BY CountryName";

		using var cnn = dbhelper.GetDatabaseConnection();
		return await cnn.QueryAsync<CountryModel>(sql, new { groups });
	}

	public async Task<IEnumerable<CountryModel>> GetCountriesInStage(long userid, int stageid)
	{
		string sql =
			@"SELECT *
			FROM Countries
				INNER JOIN Answers ON Countries.CountryID = Answers.CountryID
			WHERE Answers.StageID = @stageid
			AND Answers.UserID = @userid";

		using var cnn = dbhelper.GetDatabaseConnection();
		return await cnn.QueryAsync<CountryModel>(sql, new { userid, stageid });
	}
}
