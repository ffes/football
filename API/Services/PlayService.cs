using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Football.Models;
using Football.Repositories;
using Microsoft.Extensions.Options;

namespace Football.Services;

public class PlayService(
	IOptions<SystemSettings> systemSettings,
	IMatchesRepository matchesRepository,
	IAnswersRepository answersRepository
	): IPlayService
{
	private readonly SystemSettings _systemSettings = systemSettings.Value;

	public async Task<IEnumerable<UserModel>> GetStandings()
	{
		return await answersRepository.GetStandings();
	}

	public async Task<IEnumerable<MatchModel>> GetMatchesForGroups()
	{
		return await matchesRepository.GetMatchesInGroups();
	}

	public async Task<IEnumerable<MatchModel>> GetMatchesForKnockout()
	{
		return await matchesRepository.GetKnockoutStageMatches();
	}

	public async Task<bool> SetMatchResult(long matchid, int result)
	{
		return await matchesRepository.SetMatchResult(matchid, result);
	}

	public async Task<bool> SetMatchHome(int matchid, string country)
	{
		return await matchesRepository.SetMatchHome(matchid, country);
	}

	public async Task<bool> SetMatchAway(int matchid, string country)
	{
		return await matchesRepository.SetMatchAway(matchid, country);
	}

	public async Task RecalcStandings()
	{
		await answersRepository.ResetPoint();
		await answersRepository.SetPointsForGroupStage();
		await answersRepository.SetPointsForKoStage();
	}

	public async Task<bool> InKoStages()
	{
		var ko = await matchesRepository.GetFirstKoMatchDate();
		return ko.AddDays(-1) < DateTime.Now;
	}

	public async Task<IEnumerable<MatchModel>> GetUserMatches(int userid)
	{
		return await answersRepository.GetUserMatches(userid);
	}

	public async Task<IEnumerable<UserKnockoutModel>> GetUserKnockout(int userid)
	{
		return await answersRepository.GetUserKnockout(userid);
	}
}
