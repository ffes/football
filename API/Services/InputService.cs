using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Football.Models;
using Football.Repositories;
using Microsoft.Extensions.Options;

namespace Football.Services;

public class InputService(
	IOptions<SystemSettings> systemSettings,
	IAnswersRepository answersRepository,
	IMatchesRepository matchesRepository,
	ICountriesRepository countriesRepository
	): IInputService
{
	private readonly SystemSettings _systemSettings = systemSettings.Value;

	public async Task<bool> InputAllowed()
	{
		var firstMatch = await matchesRepository.GetFirstMatchDate();
		return DateTime.Now < firstMatch.AddHours(-1 * Math.Abs(_systemSettings.InputBeforeFirstMatchInHours));
	}

	public async Task<bool> AddAnswerForMatch(long userid, long matchid, int prediction)
	{
		// First delete the answer for this match
		await answersRepository.DeleteAnswerForMatch(userid, matchid);

		// If there is no useful answer given, don't store it
		if (prediction == 0)
			return false;

		// Store the answer
		return await answersRepository.AddAnswerForMatch(userid, matchid, prediction);
	}

	public async Task<bool> AddAnswerForStage(long userid, int stageid, string countryid, string formid)
	{
		// First delete the answer for this formid
		await answersRepository.DeleteAnswerForStage(userid, stageid, formid);

		// If there is no useful answer given, don't store it
		if (string.IsNullOrWhiteSpace(countryid))
			return false;

		// Check if the country has been used already in this stage
		if (await answersRepository.IsCountryInStage(userid, stageid, countryid))
			return false;

		// Store the answer
		return await answersRepository.AddAnswerForStage(userid, stageid, countryid, formid);
	}

	public async Task<IEnumerable<MatchModel>> GetGroup(long userid, string group)
	{
		return await matchesRepository.GetMatchesInGroup(userid, group.ToUpper());
	}

	public async Task<IEnumerable<GroupModel>> GetGroups(long userid)
	{
		var list = new List<GroupModel>();
		GroupModel group = null;
		string lastGroup = "";

		var countries = await countriesRepository.GetCountries();
		var answers = await answersRepository.GetNumberOfAnswersPerUserInGroupStage(userid);

		foreach (var country in countries)
		{
			if (country.GroupID != lastGroup)
			{
				group = new GroupModel
				{
					GroupID = country.GroupID,
					Countries = []
				};

				foreach (var a in answers)
				{
					if (a.GroupID == country.GroupID)
					{
						group.Completed = (a.GivenAnswers == a.PossibleAnswers);
						break;
					}
				}

				list.Add(group);
			}
			group.Countries.Add(country);
			lastGroup = country.GroupID;
		}

		return list;
	}

	public async Task<IEnumerable<StageModel>> GetKnockoutStages(long userid)
	{
		return await matchesRepository.GetKnockoutStages(userid);
	}

	public async Task<IEnumerable<StageMatchModel>> GetKnockoutStage(long userid, int stageid)
	{
		// Get the matches
		var matches = await matchesRepository.GetKnockoutStage(stageid);
		if (!matches.Any())
			return null;

		// The "champion" stage only has 1 answer.
		// That shouldn't get cloned
		bool clone = (matches.First().Answers > 1);

		// Now fill and duplicate the matches
		var matchesToAdd = new List<StageMatchModel>();
		foreach (var match in matches)
		{
			match.MatchIsHome = true;

			if (clone)
			{
				var m2 = match.Clone();
				matchesToAdd.Add(m2);
				match.MatchIsHome = false;
			}
		}

		// Get the answers given by the user
		var answers = await matchesRepository.GetKnockoutStage(userid, stageid);

		// Match the give answers to the matches
		var m = matches.Concat(matchesToAdd).OrderBy(match => match.From);

		// FIXME: There must be a more efficient way to do this
		foreach (var answer in answers)
		{
			foreach (var match in m)
			{
				if (match.FormID.Equals(answer.FormID, System.StringComparison.OrdinalIgnoreCase))
				{
					match.CountryID = answer.CountryID;
					match.CountryName = answer.CountryName;
					break;
				}
			}
		}

		return m;
	}

	public string[] GetGroupsFromFrom(string from)
	{
		var groups = new List<string>();

		// Go through the string
		foreach (char c in from)
		{
			// Is it a letter?
			if (char.IsLetter(c))
			{
				// Add it to the list
				groups.Add(c.ToString().ToUpper());
			}
		}

		return groups.ToArray();
	}

	public async Task<IEnumerable<CountryModel>> GetCountriesFrom(string from)
	{
		if (from?.Length <= 1)
			return null;

		if (from.StartsWith('W'))
		{
			if (!int.TryParse(from.Substring(1), out int matchid))
				return null;

			var match = await matchesRepository.GetMatchById(matchid);

			return [
				new CountryModel
				{
					CountryID = match.HomeID,
					CountryName = match.HomeName
				},
				new CountryModel {
					CountryID = match.AwayID,
					CountryName = match.AwayName
				}
			];
		}

		if (char.IsDigit(from[0]))
		{
			var groups = GetGroupsFromFrom(from);
			return await countriesRepository.GetCountriesInGroups(groups);
		}

		return null;
	}

	public async Task<IEnumerable<CountryModel>> GetCountriesFromStage(long userid, int stageid)
	{
		return await countriesRepository.GetCountriesInStage(userid, stageid);
	}
}
