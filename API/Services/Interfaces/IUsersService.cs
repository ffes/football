using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Football.Models;

namespace Football.Services;

public interface IUsersService
{
	public Task<UserModel> Authenticate(string email, string password);
	public Task<bool> SignUp(SignUpModel user);
	public IList<Claim> BuildClaims( UserModel user );
	public Task<bool> EnableUser(int id, bool enable);
	public Task<UserModel> GetUserByIdentity(ClaimsPrincipal user);
	public Task<IEnumerable<UserModel>> GetUsers();
}
