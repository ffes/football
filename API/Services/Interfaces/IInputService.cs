using System.Collections.Generic;
using System.Threading.Tasks;
using Football.Models;

namespace Football.Services;

public interface IInputService
{
	public Task<bool> InputAllowed();
	public Task<bool> AddAnswerForMatch(long userid, long matchid, int prediction);
	public Task<bool> AddAnswerForStage(long userid, int stageid, string countryid, string formid);
	public Task<IEnumerable<MatchModel>> GetGroup(long userid, string group);
	public Task<IEnumerable<GroupModel>> GetGroups(long userid);
	public Task<IEnumerable<StageModel>> GetKnockoutStages(long userid);
	public Task<IEnumerable<StageMatchModel>> GetKnockoutStage(long userid, int stageid);
	public Task<IEnumerable<CountryModel>> GetCountriesFrom(string from);
	public Task<IEnumerable<CountryModel>> GetCountriesFromStage(long userid, int stageid);
}
