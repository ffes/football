using System.Collections.Generic;
using System.Threading.Tasks;
using Football.Models;

namespace Football.Services;

public interface IPlayService
{
	public Task<IEnumerable<UserModel>> GetStandings();
	public Task RecalcStandings();
	public Task<IEnumerable<MatchModel>> GetMatchesForGroups();
	public Task<IEnumerable<MatchModel>> GetMatchesForKnockout();
	public Task<bool> InKoStages();
	public Task<bool> SetMatchResult(long matchid, int result);
	public Task<bool> SetMatchHome(int matchid, string country);
	public Task<bool> SetMatchAway(int matchid, string country);
	public Task<IEnumerable<MatchModel>> GetUserMatches(int userid);
	public Task<IEnumerable<UserKnockoutModel>> GetUserKnockout(int userid);
}
