using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Football.Helpers;
using Football.Models;
using Football.Models.Exceptions;
using Football.Repositories;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.JsonWebTokens;

namespace Football.Services;

public class UsersService(
	IUsersRepository usersRepository,
	ILogger<UsersService> logger): IUsersService
{
	public async Task<UserModel> Authenticate(string email, string password)
	{
		var user = await usersRepository.GetUserByEmail(email);

		// Danger, user existence timing attack
		if (default == user)
			throw new UserNotFoundException();

		if (!user.Enabled)
			throw new UserNotEnabledException();

		if (string.IsNullOrWhiteSpace(user.Password))
			throw new InvalidPasswordException();

		var split = user.Password.Split("|");

		if (split.Length != 3)
			throw new InvalidPasswordException();

		var version = split[0];
		var salt = Convert.FromBase64String(split[1]);
		var hash = Convert.FromBase64String(split[2]);

		if (!HashHelper.ValidatePassword(password, salt, hash))
			throw new InvalidPasswordException();

		return user;
	}

	public async Task<bool> SignUp(SignUpModel user)
	{
		// Generate a cryptographically secure random guid for user activation
		user.ActivationKey = (GuidHelper.CreateCryptographicallySecureRandomRFC4122Guid()).ToString("D");

		var (salt, hash) = HashHelper.HashPassword(user.Password);

		user.Password = string.Join("|", new string[]
		{
			"1",
			Convert.ToBase64String(salt),
			Convert.ToBase64String(hash),
		});

		return await usersRepository.SaveSignup(user);
	}

	public async Task<bool> EnableUser(int id, bool enable)
	{
		var user = await usersRepository.GetUserById(id);

		if (default == user)
			throw new UserNotFoundException();

		return await usersRepository.EnableUser(id, enable);
	}

	public async Task<UserModel> GetUserByIdentity(ClaimsPrincipal identity)
	{
		if (default == identity || !(identity?.Identity?.IsAuthenticated ?? false) )
			throw new InvalidSessionException();

		var user = GetUserModel(identity);

		return await Task.FromResult(user);
	}

	public async Task<IEnumerable<UserModel>> GetUsers()
	{
		return await usersRepository.GetUsers();
	}

	public IList<Claim> BuildClaims( UserModel user )
	{
		ArgumentNullException.ThrowIfNull(user, "User model is undefined");

		var claims = new List<Claim>
		{
			new (JwtRegisteredClaimNames.Sub, user.UserID.ToString(), ClaimValueTypes.Integer64 ),
			new (JwtRegisteredClaimNames.Name, user.RealName ?? ""),
			new (JwtRegisteredClaimNames.GivenName, user.NickName ?? ""),
			new (JwtRegisteredClaimNames.Email, user.EMail ?? ""),
			new (JwtExtendedClaimNames.UserLevel, user.UserLevel.ToString(), ClaimValueTypes.Integer64),
			new (JwtExtendedClaimNames.Enabled, user.Enabled ? "true":"false", ClaimValueTypes.Boolean),
		};

		if( user.UserLevel > 99 )
			claims.Add(new Claim(ClaimTypes.Role, Roles.Admin ));

		if( user.UserLevel > 0 )
			claims.Add(new Claim(ClaimTypes.Role, Roles.User ));

		return claims;
	}

	public UserModel GetUserModel(ClaimsPrincipal identity)
	{
		var subject = identity.FindFirst(c => c.Type.Equals(JwtRegisteredClaimNames.Sub, StringComparison.InvariantCultureIgnoreCase));
		var name = identity.FindFirst(c => c.Type.Equals(JwtRegisteredClaimNames.Name, StringComparison.InvariantCultureIgnoreCase));
		var given_name = identity.FindFirst(c => c.Type.Equals(JwtRegisteredClaimNames.GivenName, StringComparison.InvariantCultureIgnoreCase));
		var email = identity.FindFirst(c => c.Type.Equals(JwtRegisteredClaimNames.Email, StringComparison.InvariantCultureIgnoreCase));
		var user_level = identity.FindFirst(c => c.Type.Equals(JwtExtendedClaimNames.UserLevel, StringComparison.InvariantCultureIgnoreCase));
		var enabled = identity.FindFirst(c => c.Type.Equals(JwtExtendedClaimNames.Enabled, StringComparison.InvariantCultureIgnoreCase));

		if (default == subject || default == name || default == given_name || default == user_level )
			throw new InvalidSessionException();

		if( int.TryParse(subject.Value, out var userid) ) {
			return new UserModel()
			{
				UserID = userid,
				RealName = name?.Value ?? "",
				NickName = given_name?.Value ?? "",
				EMail = email?.Value ?? "",
				Enabled = enabled?.Value == "true",
				UserLevel = int.Parse(user_level?.Value)
			};
		}

		return null;
	}
}
