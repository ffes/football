using System;
using System.IO;
using System.Net.Mime;
using System.Reflection;
using System.Threading.Tasks;
using Football.Helpers;
using Football.Models;
using Football.Repositories;
using Football.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption;
using Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption.ConfigurationModel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Football;

public class Startup(IConfiguration configuration)
{
	// This method gets called by the runtime. Use this method to add services to the container.
	public void ConfigureServices(IServiceCollection services)
	{
		var keystorage = configuration.GetConnectionString("KeyStorage");

		var dp = services.AddDataProtection();

		if (!string.IsNullOrWhiteSpace(keystorage) && Directory.Exists(keystorage))
		{
			dp
				.UseCryptographicAlgorithms(
					new AuthenticatedEncryptorConfiguration
					{
						EncryptionAlgorithm = EncryptionAlgorithm.AES_256_GCM,
						ValidationAlgorithm = ValidationAlgorithm.HMACSHA256
					})
				.PersistKeysToFileSystem(new DirectoryInfo(keystorage));
		}

		services.AddControllers();

		services.AddSwaggerGen(c =>
		{
			c.SwaggerDoc("v1", new OpenApiInfo {
				Title = "Football",
				Version = "v1"
			});

			// Use method name as operationId
			c.CustomOperationIds(apiDesc =>
			{
				return apiDesc.TryGetMethodInfo(out MethodInfo methodInfo) ? methodInfo.Name : null;
			});
		});

		services.AddMvc(options =>
		{
			// All endpoints produce JSON, so set it once.
			// This reduces the number of output types to one,
			// which is useful in the generated code for the client.
			options.Filters.Add(new ProducesAttribute(MediaTypeNames.Application.Json));
		});

		// Add CORS support to the services
		var appsettings = configuration.Get<AppSettings>();
		services.AddCors(options =>
		{
			options.AddDefaultPolicy(policy =>
			{
				policy
					.WithOrigins(appsettings.Cors.AcceptedOrigins)
					.SetPreflightMaxAge(new TimeSpan(0, 0, appsettings.Cors.PreFlightMaxAge))
					.AllowAnyMethod()
					.AllowAnyHeader()
					.AllowCredentials();
			});
		});

		// Add authentication
		services.AddAuthentication().AddCookie(options =>
		{
			options.Cookie.IsEssential = true;
			options.Cookie.HttpOnly = true;
			options.Cookie.Name = "football_session";
			options.Cookie.MaxAge = new TimeSpan( 336, 0, 0 );

#if DEBUG
			options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
			options.Cookie.SameSite = SameSiteMode.Lax;
#else
			options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
			options.Cookie.SameSite = SameSiteMode.Strict;
#endif

			// https://github.com/dotnet/aspnetcore/issues/9039
			options.Events.OnRedirectToAccessDenied = context => {
				context.Response.StatusCode = 403;
				return Task.CompletedTask;
			};
			options.Events.OnRedirectToLogin = context => {
				context.Response.StatusCode = 401;
				return Task.CompletedTask;
			};

			options.AccessDeniedPath = null;
			options.LoginPath = null;
			options.LogoutPath = null;
		});

		// Set the options for routing
		services.AddRouting(options => options.LowercaseUrls = true);

		// Add the sections from appsettings
		services.Configure<ConnectionStringSettings>(configuration.GetSection(ConnectionStringSettings.ConnectionStrings));
		services.Configure<SystemSettings>(configuration.GetSection(SystemSettings.System));
		services.Configure<CorsSettings>(configuration.GetSection(CorsSettings.Cors));

		// Add the repositories
		services.AddTransient<IAnswersRepository, AnswersRepository>();
		services.AddTransient<ICountriesRepository, CountriesRepository>();
		services.AddTransient<IMatchesRepository, MatchesRepository>();
		services.AddTransient<IUsersRepository, UsersRepository>();

		// Add the services
		services.AddTransient<IInputService, InputService>();
		services.AddTransient<IPlayService, PlayService>();
		services.AddTransient<IUsersService, UsersService>();

		// Add the helpers
		services.AddTransient<IDatabaseHelper, DatabaseHelper>();
	}

	// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
	public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
	{
		if (env.IsDevelopment())
		{
			app.UseDeveloperExceptionPage();
			app.UseSwagger();
			app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Football v1"));
		}

#if !DEBUG
		app.UseHttpsRedirection();
#endif

		app.UseRouting();
		app.UseCors();

		app.UseAuthentication();
		app.UseAuthorization();

		app.UseEndpoints(endpoints =>
		{
			endpoints.MapControllers();
		});
	}
}
