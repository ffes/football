using System.Collections.Generic;

namespace Football.Models;

public class GroupModel
{
	public string GroupID { get; set; }
	public List<CountryModel> Countries { get; set; }
	public bool Completed { get; set; }
}

public class AnswersInGroupStage
{
	public string GroupID { get; set; }
	public int PossibleAnswers { get; set; }
	public int GivenAnswers { get; set; }
}
