using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Football.Models;

public class UserModel
{
	[Required]
	public int UserID { get; set; }
	public string EMail { get; set; }

	[JsonIgnore]
	public string Password { get; set; }
	public string RealName { get; set; }
	public string NickName { get; set; }

	[JsonIgnore]
	[BindNever]
	public string SigninKey{ get; set; }
	public bool Enabled { get; set; }
	public int UserLevel { get; set; }
	public int Score { get; set; }
	public int Answers { get; set; }
}

public class AuthenticateModel
{
	[Required]
	[StringLength(100, MinimumLength = 2)]
	public string EMail { get; set; }

	[Required]
	[StringLength(100, MinimumLength = 2)]
	public string Password { get; set; }
}

public class SignUpModel
{
	[Required]
	[StringLength(100, MinimumLength = 2)]
	public string EMail { get; set; }

	[Required]
	[StringLength(100, MinimumLength = 8)]
	public string Password { get; set; }

	[Required]
	[StringLength(100, MinimumLength = 2)]
	public string RealName { get; set; }

	[Required]
	[StringLength(100, MinimumLength = 2)]
	public string NickName { get; set; }

	[JsonIgnore]
	[BindNever]
	public string ActivationKey { get; set; }
}

public abstract class JwtExtendedClaimNames
{
	public const string UserLevel = "user_level";
	public const string Enabled = "user_enabled";
}

public class UserKnockoutModel
{
	public string RealName { get; set; }
	public string StageName { get; set; }
	public string CountryID { get; set; }
	public string CountryName { get; set; }
	public int Points { get; set; }
}
