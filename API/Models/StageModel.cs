using System.Text.Json;
using System.Text.Json.Serialization;

namespace Football;

public class StageModel
{
	public int StageID { get; set; }
	public string StageName { get; set; }
	public bool Knockout { get; set; }
	public int Answers { get; set; }
	public int Points { get; set; }
	public bool Completed { get; set; }
}

public class StageMatchModel
{
	public int StageID { get; set; }
	public string StageName { get; set; }
	public int Answers { get; set; }
	public string CountryID { get; set; }
	public string CountryName { get; set; }
	public int MatchID { get; set; }
	public bool MatchIsHome { get; set; }
	[JsonIgnore]
	public string HomeFrom { private get; set; }
	[JsonIgnore]
	public string AwayFrom { private get; set; }

	private string _formID;
	public string FormID
	{
		get
		{
			if (string.IsNullOrWhiteSpace(_formID))
				return (MatchIsHome ? "h" : "a") + MatchID.ToString();
			return _formID;
		}
		set
		{
			_formID = value;
		}
	}

	public string From
	{
		get
		{
			return MatchIsHome ? HomeFrom : AwayFrom;
		}
	}

	public StageMatchModel Clone()
	{
		var clone = JsonSerializer.Serialize(this);
		return JsonSerializer.Deserialize<StageMatchModel>(clone);
	}
}

public class StageAnswerModel
{
	public string CountryID { get; set; }
	public string FormID { get; set; }
}
