using System;

namespace Football.Models.Exceptions;


public class InvalidPasswordException : ApplicationException
{
	public InvalidPasswordException() { }
	public InvalidPasswordException(string message): base(message) { }
	public InvalidPasswordException(string message, Exception inner): base(message, inner) { }
}

public class InvalidSessionException : ApplicationException
{
	public InvalidSessionException() { }
	public InvalidSessionException(string message): base(message) { }
	public InvalidSessionException(string message, Exception inner): base(message, inner) { }
}
