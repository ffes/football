namespace Football.Models.Exceptions;

using System;

public class UserNotFoundException : ApplicationException
{
	public UserNotFoundException() { }
	public UserNotFoundException(string message): base(message) { }
	public UserNotFoundException(string message, Exception inner): base(message, inner) { }
}

public class UserNotEnabledException : ApplicationException
{
	public UserNotEnabledException() { }
	public UserNotEnabledException(string message): base(message) { }
	public UserNotEnabledException(string message, Exception inner): base(message, inner) { }
}
