using System;

namespace Football.Models;

[Serializable]
public enum DatabaseType
{
	SqlServer,
	Sqlite
}

public class ConnectionStringSettings
{
	public const string ConnectionStrings = "ConnectionStrings";
	public string SqlServer { get; set; }
	public string Sqlite { get; set; }
}

public class SystemSettings
{
	public const string System = "System";
	public DatabaseType DatabaseType { get; set; }
	public int InputBeforeFirstMatchInHours { get; set; }
}

public class CorsSettings
{
	public const string Cors = "CORS";
	public string[] AcceptedOrigins { get; set; }
	public int PreFlightMaxAge { get; set; }
}

public class AppSettings
{
	public ConnectionStringSettings ConnectionStrings { get; set; }
	public CorsSettings Cors { get; set; }
	public SystemSettings System { get; set; }
}
