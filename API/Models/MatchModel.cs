using System;

namespace Football.Models;

public class MatchModel
{
	public int MatchID { get; set; }
	public string GroupID { get; set; }
	public int StageID { get; set; }
	public string StageName { get; set; }
	public string HomeID { get; set; }
	public string HomeName { get; set; }
	public string AwayID { get; set; }
	public string AwayName { get; set; }
	public DateTime MatchDate { get; set; }
	public int MatchResult { get; set; }
	public string HomeFrom { get; set; }
	public string AwayFrom { get; set; }
	public string Stadium { get; set; }
	public int Answer { get; set; }
	public string RealName { get; set; }
}
