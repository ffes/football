namespace Football.Models;

public class CountryModel
{
	public string CountryID { get; set; }
	public string CountryName { get; set; }
	public string GroupID { get; set; }
}
