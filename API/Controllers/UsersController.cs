using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Football.Models;
using Football.Models.Exceptions;
using Football.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Football.Controllers;

[Authorize]
[ApiController]
[Route("[controller]")]
public class UsersController(
	IUsersService usersService,
	ILogger<InputController> logger): ControllerBase
{

	[HttpGet("activate")]
	[Produces("application/json", Type = typeof(UserModel))]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	public async Task<IActionResult> Activate()
	{
		try
		{
			var user = await usersService.GetUserByIdentity(this.User);
			return Ok(user);
		}
		catch (InvalidSessionException ex)
		{
			return Unauthorized("Invalid session");
		}
	}

	[AllowAnonymous]
	[HttpPost("authenticate")]
	[Produces("application/json", Type = typeof(UserModel))]
	[ProducesResponseType(StatusCodes.Status400BadRequest)]
	public async Task<IActionResult> Authenticate([FromBody] AuthenticateModel model)
	{
		if (!ModelState.IsValid)
			return ValidationProblem();

		try
		{
			var user = await usersService.Authenticate(model.EMail, model.Password);

			if (user == null)
				return BadRequest(new { message = "E-Mail address or password is incorrect" });

			var claims = usersService.BuildClaims(user);
			var userIdentity = new ClaimsIdentity(claims, "Credentials");
			var userPrincipal = new ClaimsPrincipal(userIdentity);

			await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, userPrincipal,
				new AuthenticationProperties
				{
					ExpiresUtc = DateTime.UtcNow.AddHours(336),
					IsPersistent = true,
					AllowRefresh = true,
				});

			return Ok(user);
		}
		catch (UserNotEnabledException ex)
		{
			return Unauthorized("User is not enabled");
		}
		catch (ApplicationException ex ) when ( ex is UserNotFoundException or InvalidPasswordException)
		{
			return Unauthorized("Invalid user or password");
		}
	}

	[AllowAnonymous]
	[HttpPost("signup")]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(StatusCodes.Status400BadRequest)]
	public async Task<IActionResult> SignUp([FromBody] SignUpModel user)
	{
		if (user == null)
			ModelState.AddModelError("", "No signup details specified");

		if (!ModelState.IsValid)
			return ValidationProblem();

		var res = await usersService.SignUp(user);

		return res ? NoContent() : BadRequest();
	}


	[HttpGet("")]
	[Authorize(Roles = Roles.Admin)]
	[Produces("application/json", Type = typeof(IEnumerable<UserModel>))]
	public async Task<IActionResult> GetUsers()
	{
		var result = await usersService.GetUsers();

		return Ok(result);
	}


	[HttpPost("signout")]
	[Authorize]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	public async Task<IActionResult> Signout()
	{
		await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

		return NoContent();
	}

	[HttpPost("{id:int}/enable")]
	[Authorize(Roles = Roles.Admin)]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(StatusCodes.Status400BadRequest)]
	[ProducesResponseType(StatusCodes.Status404NotFound)]
	public async Task<IActionResult> EnableUser([FromRoute] int id)
	{
		if (id < 1)
			ModelState.AddModelError("", "Invalid user id specified");

		if (!ModelState.IsValid)
			return ValidationProblem();

		try
		{
			var result = await usersService.EnableUser(id, true);

			return result ? NoContent() : BadRequest();
		}
		catch (UserNotFoundException ex)
		{
			return NotFound("User not found");
		}
	}

	[HttpPost("{id:int}/disable")]
	[Authorize(Roles = Roles.Admin)]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(StatusCodes.Status400BadRequest)]
	[ProducesResponseType(StatusCodes.Status404NotFound)]
	public async Task<IActionResult> DisableUser([FromRoute] int id)
	{
		if (id < 1)
			ModelState.AddModelError("", "Invalid user id specified");

		if (!ModelState.IsValid)
			return ValidationProblem();

		try
		{
			var result = await usersService.EnableUser(id, false);

			return result ? NoContent() : BadRequest();
		}
		catch (UserNotFoundException ex)
		{
			return NotFound("User not found");
		}
	}
}
