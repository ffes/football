using System.Collections.Generic;
using System.Threading.Tasks;
using Football.Models;
using Football.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Football.Controllers;

[Authorize]
[ApiController]
[Route("[controller]")]
public class PlayController(
	IPlayService playService,
	ILogger<InputController> logger): ControllerBase
{
	[AllowAnonymous]
	[HttpGet("standings")]
	[Produces("application/json", Type = typeof(IEnumerable<UserModel>))]
	[ProducesResponseType(StatusCodes.Status404NotFound)]
	public async Task<IEnumerable<UserModel>> GetStandings()
	{
		return await playService.GetStandings();
	}

	[HttpGet("standings/recalc")]
	[Authorize(Roles = Roles.Admin)]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	public async Task<IActionResult> RecalcStandings()
	{
		await playService.RecalcStandings();
		return NoContent();
	}

	[HttpGet("ko")]
	[Produces("application/json", Type = typeof(bool))]
	public async Task<bool> InKoStages()
	{
		return await playService.InKoStages();
	}

	[HttpGet("matches/groups")]
	[Authorize(Roles = Roles.Admin)]
	[Produces("application/json", Type = typeof(IEnumerable<MatchModel>))]
	[ProducesResponseType(StatusCodes.Status404NotFound)]
	public async Task<IEnumerable<MatchModel>> GetMatchesForGroups()
	{
		return await playService.GetMatchesForGroups();
	}

	[HttpGet("matches/ko")]
	[Authorize(Roles = Roles.Admin)]
	[Produces("application/json", Type = typeof(IEnumerable<MatchModel>))]
	[ProducesResponseType(StatusCodes.Status404NotFound)]
	public async Task<IEnumerable<MatchModel>> GetMatchesForKnockout()
	{
		return await playService.GetMatchesForKnockout();
	}

	[HttpPatch("match/{matchid}/result")]
	[Authorize(Roles = Roles.Admin)]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status404NotFound)]
	public async Task<IActionResult> PatchMatchResult(long matchid, [FromBody] int result)
	{
		logger.LogInformation("PatchMatchResult: MatchID {matchid} => {result}", matchid, result);
		var res = await playService.SetMatchResult(matchid, result);
		return res ? NoContent() : NotFound();
	}

	[HttpPatch("match/{matchid}/home/{country}")]
	[Authorize(Roles = Roles.Admin)]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status404NotFound)]
	public async Task<IActionResult> PatchMatchHome(int matchid, string country)
	{
		logger.LogInformation("PatchMatchHome: MatchID {matchid} => {country}", matchid, country);
		var res = await playService.SetMatchHome(matchid, country);
		return res ? NoContent() : NotFound();
	}

	[HttpPatch("match/{matchid}/away/{country}")]
	[Authorize(Roles = Roles.Admin)]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	[ProducesResponseType(StatusCodes.Status404NotFound)]
	public async Task<IActionResult> PatchMatchAway(int matchid, string country)
	{
		logger.LogInformation("PatchMatchAway: MatchID {matchid} => {country}", matchid, country);
		var res = await playService.SetMatchAway(matchid, country);
		return res ? NoContent() : NotFound();
	}

	[HttpGet("user/{userid}/groups")]
	[Produces("application/json", Type = typeof(IEnumerable<MatchModel>))]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status404NotFound)]
	public async Task<IEnumerable<MatchModel>> GetUserGroups(int userid)
	{
		return await playService.GetUserMatches(userid);
	}

	[HttpGet("user/{userid}/ko")]
	[Produces("application/json", Type = typeof(IEnumerable<UserKnockoutModel>))]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status404NotFound)]
	public async Task<IEnumerable<UserKnockoutModel>> GetUserKnockout(int userid)
	{
		return await playService.GetUserKnockout(userid);
	}
}
