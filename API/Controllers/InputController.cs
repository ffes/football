using System.Collections.Generic;
using System.Threading.Tasks;
using Football.Models;
using Football.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Football.Controllers;

[Authorize]
[ApiController]
[Route("[controller]")]
public class InputController(
	IInputService inputService,
	IUsersService usersService,
	ILogger<InputController> logger): ControllerBase
{
	[HttpGet("allowed")]
	[Produces("application/json", Type = typeof(bool))]
	public async Task<IActionResult> InputAllowed()
	{
		return Ok(await inputService.InputAllowed());
	}

	[HttpPost("answer/match/{matchid}")]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(StatusCodes.Status404NotFound)]
	public async Task<IActionResult> PostAnswerForMatch(long matchid, [FromBody] int answer)
	{
		if (!await inputService.InputAllowed())
		{
			logger.LogError("PostAnswerForStage: Input not allowed anymore");
			return NotFound();
		}

		var user = await usersService.GetUserByIdentity(this.User);
		logger.LogInformation("PostAnswer: userid = {UserID}, matchid = {matchid}, answer = {answer}", user.UserID, matchid, answer);

		bool result = await inputService.AddAnswerForMatch(user.UserID, matchid, answer);
		return result ? NoContent() : NotFound();
	}

	[HttpPost("answer/stage/{stageid:int}")]
	[ProducesResponseType(StatusCodes.Status204NoContent)]
	[ProducesResponseType(StatusCodes.Status404NotFound)]
	public async Task<IActionResult> PostAnswerForStage(int stageid, [FromBody] StageAnswerModel answer)
	{
		if (!await inputService.InputAllowed())
		{
			logger.LogError("PostAnswerForStage: Input not allowed anymore");
			return NotFound();
		}

		var user = await usersService.GetUserByIdentity(this.User);
		logger.LogInformation("PostAnswerForStage: userid = {UserID}, stageid = {stageid}, countryid = {countyid}, formid = {formid}", user.UserID, stageid, answer.CountryID, answer.FormID);

		bool result = await inputService.AddAnswerForStage(user.UserID, stageid, answer.CountryID, answer.FormID);
		return result ? NoContent() : NotFound();
	}

	[HttpGet(@"group/{groupid:regex(^[[A-Fa-f]]$)}")]
	[Produces("application/json", Type = typeof(IEnumerable<MatchModel>))]
	[ProducesResponseType(StatusCodes.Status404NotFound)]
	public async Task<IEnumerable<MatchModel>> GetGroup(string groupid)
	{
		if (!await inputService.InputAllowed())
		{
			logger.LogError("PostAnswerForStage: Input not allowed anymore");
			return null;
		}

		var user = await usersService.GetUserByIdentity(this.User);
		logger.LogInformation("GetGroup: groupid = {groupid}", groupid);
		return await inputService.GetGroup(user.UserID, groupid);
	}

	[HttpGet("groups")]
	[Produces("application/json", Type = typeof(IEnumerable<GroupModel>))]
	[ProducesResponseType(StatusCodes.Status404NotFound)]
	public async Task<IEnumerable<GroupModel>> GetGroups()
	{
		if (!await inputService.InputAllowed())
		{
			logger.LogError("PostAnswerForStage: Input not allowed anymore");
			return null;
		}

		logger.LogInformation("GetGroups");
		var user = await usersService.GetUserByIdentity(this.User);
		return await inputService.GetGroups(user.UserID);
	}

	[HttpGet("countries/from/{from}")]
	[Produces("application/json", Type = typeof(IEnumerable<GroupModel>))]
	[ProducesResponseType(StatusCodes.Status404NotFound)]
	public async Task<IEnumerable<CountryModel>> GetCountriesFrom(string from)
	{
		return await inputService.GetCountriesFrom(from);
	}

	[HttpGet("countries/stage/{stageid}")]
	[Produces("application/json", Type = typeof(IEnumerable<GroupModel>))]
	[ProducesResponseType(StatusCodes.Status404NotFound)]
	public async Task<IEnumerable<CountryModel>> GetCountriesFromStage(int stageid)
	{
		if (!await inputService.InputAllowed())
		{
			logger.LogError("PostAnswerForStage: Input not allowed anymore");
			return null;
		}

		var user = await usersService.GetUserByIdentity(this.User);
		return await inputService.GetCountriesFromStage(user.UserID, stageid);
	}

	[HttpGet("ko")]
	[Produces("application/json", Type = typeof(IEnumerable<StageModel>))]
	[ProducesResponseType(StatusCodes.Status404NotFound)]
	public async Task<IEnumerable<StageModel>> GetKoStages()
	{
		logger.LogInformation("GetKoStages");
		var user = await usersService.GetUserByIdentity(this.User);
		return await inputService.GetKnockoutStages(user.UserID);
	}

	[HttpGet("ko/{stageid}")]
	[Produces("application/json", Type = typeof(IEnumerable<StageMatchModel>))]
	[ProducesResponseType(StatusCodes.Status404NotFound)]
	public async Task<IEnumerable<StageMatchModel>> GetKoStage(int stageid)
	{
		if (!await inputService.InputAllowed())
		{
			logger.LogError("PostAnswerForStage: Input not allowed anymore");
			return null;
		}

		logger.LogInformation("GetKoStage: stageid = {stageid}", stageid);
		var user = await usersService.GetUserByIdentity(this.User);
		return await inputService.GetKnockoutStage(user.UserID, stageid);
	}
}
