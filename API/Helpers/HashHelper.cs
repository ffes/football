using System;
using System.Linq;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace Football.Helpers;

public class HashHelper
{
	private const int IterationCount = 210_000;
	private const KeyDerivationPrf HashingAlgorithm = KeyDerivationPrf.HMACSHA512;
	private const int HashLengthBytes = 256 / 8;

	public static (byte[], byte[]) HashPassword(string key)
	{
		var salt = RandomNumberGenerator.GetBytes(128 / 8); // divide by 8 to convert bits to bytes

		var hash = KeyDerivation.Pbkdf2(
			password: key,
			salt: salt,
			prf: HashingAlgorithm,
			iterationCount: IterationCount,
			numBytesRequested: HashLengthBytes);

		return (salt, hash);
	}

	public static bool ValidatePassword(string key, byte[] salt, byte[] hash)
	{
		ArgumentNullException.ThrowIfNullOrWhiteSpace(key);
		ArgumentNullException.ThrowIfNull(salt);
		ArgumentNullException.ThrowIfNull(hash);

		if (salt.Length < 1)
			throw new ArgumentException("Salt cannot be empty", nameof(salt));

		if (hash.Length < 1)
			throw new ArgumentException("Hash cannot be empty", nameof(hash));

		var test_hash = KeyDerivation.Pbkdf2(
			password: key,
			salt: salt,
			prf: HashingAlgorithm,
			iterationCount: IterationCount,
			numBytesRequested: HashLengthBytes);

		return test_hash.SequenceEqual(hash);
	}
}
