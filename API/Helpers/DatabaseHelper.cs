using System.Data;
using Football.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Football.Helpers;

public class DatabaseHelper(
	ILogger<DatabaseHelper> logger,
	IOptions<ConnectionStringSettings> connectionStringOptions,
	IOptions<SystemSettings> systemOptions
	) : IDatabaseHelper
{
	private readonly ConnectionStringSettings _connectionSettings = connectionStringOptions.Value;
	private readonly SystemSettings _systemSettings = systemOptions.Value;

	public IDbConnection GetDatabaseConnection()
	{
		return _systemSettings.DatabaseType switch
		{
			DatabaseType.SqlServer => new SqlConnection(_connectionSettings.SqlServer),
			DatabaseType.Sqlite => new SqliteConnection(_connectionSettings.Sqlite),
			_ => null,
		};
	}
}
