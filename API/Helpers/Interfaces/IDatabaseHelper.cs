using System.Data;

namespace Football.Helpers;

public interface IDatabaseHelper
{
	IDbConnection GetDatabaseConnection();
}
