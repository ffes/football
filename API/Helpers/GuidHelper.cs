namespace Football.Helpers;

using System;
using System.Security.Cryptography;

public static class GuidHelper
{
	/// <summary>
	/// Generates a cryptographically secure random Guid.
	///
	/// Characteristics
	///     - GUID Variant: RFC 4122
	///     - GUID Version: 4
	///     - .NET 5
	/// RFC
	///     https://tools.ietf.org/html/rfc4122#section-4.1.3
	/// Stackoverflow
	///     https://stackoverflow.com/a/59437504/10830091
	/// </summary>
	public static Guid CreateCryptographicallySecureRandomRFC4122Guid()
	{
		// Byte indices
		int versionByteIndex = BitConverter.IsLittleEndian ? 7 : 6;
		const int variantByteIndex = 8;

		// Version mask & shift for `Version 4`
		const int versionMask = 0x0F;
		const int versionShift = 0x40;

		// Variant mask & shift for `RFC 4122`
		const int variantMask = 0x3F;
		const int variantShift = 0x80;

		// Get bytes of cryptographically-strong random values
		var bytes = new byte[16];

		RandomNumberGenerator.Fill(bytes);

		// Set version bits -- 6th or 7th byte according to Endianness, big or little Endian respectively
		bytes[versionByteIndex] = (byte) (bytes[versionByteIndex] & versionMask | versionShift);

		// Set variant bits -- 8th byte
		bytes[variantByteIndex] = (byte) (bytes[variantByteIndex] & variantMask | variantShift);

		// Initialize Guid from the modified random bytes
		return new Guid(bytes);
	}
}
