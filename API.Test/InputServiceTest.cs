using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Football.Models;
using Football.Services;
using Football.Test.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.Options;

namespace Football.Test;

[TestClass]
public class InputServiceTest
{
	[TestMethod]
	public async Task GetGroups()
	{
		var systemSettings = Options.Create(new SystemSettings());
		var countriesRepository = new MockCountriesRepository();
		var answersRepository = new MockAnswersRepository();
		var matchesRepository = new MockMatchesRepository();
		var svc = new InputService(systemSettings, answersRepository, matchesRepository, countriesRepository);
		var actual = (await svc.GetGroups(0)).ToList();

		var expected = new List<GroupModel>
		{
			new() {
				GroupID = "A",
				Countries = [
					new CountryModel {
						CountryID = "nl",
						CountryName = "Netherlands",
						GroupID = "A"
					},
					new CountryModel {
						CountryID = "de",
						CountryName = "Germany",
						GroupID = "A"
					}
				],
				Completed = false
			},
			new() {
				GroupID = "B",
				Countries = [
					new CountryModel {
						CountryID = "be",
						CountryName = "Belgium",
						GroupID = "B"
					},
					new CountryModel {
						CountryID = "fr",
						CountryName = "France",
						GroupID = "B"
					}
				],
				Completed = true
			}
		};

		Assert.AreEqual(expected.Count, actual.Count);
	}

	[DataTestMethod]
	[DataRow("1A", new string[] { "A" })]
	[DataRow("3A/B/C", new string[] { "A", "B", "C" })]
	public void GetGroupsFromFromTests(string from, string[] groups)
	{
		var systemSettings = Options.Create(new SystemSettings());
		var countriesRepository = new MockCountriesRepository();
		var answersRepository = new MockAnswersRepository();
		var matchesRepository = new MockMatchesRepository();
		var svc = new InputService(systemSettings, answersRepository, matchesRepository, countriesRepository);

		var result = svc.GetGroupsFromFrom(from);
		CollectionAssert.AreEqual(groups, result);
	}
}
