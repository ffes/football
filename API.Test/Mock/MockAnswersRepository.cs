using System.Collections.Generic;
using System.Threading.Tasks;
using Football.Models;
using Football.Repositories;

#pragma warning disable CS1998	// Since we mock, the methods aren't actually async

namespace Football.Test.Mock;

public class MockAnswersRepository: IAnswersRepository
{
	public Task<bool> AddAnswerForMatch(long userid, long matchid, int prediction)
	{
		throw new System.NotImplementedException();
	}

	public Task<bool> AddAnswerForStage(long userid, int stageid, string countryid, string formid)
	{
		throw new System.NotImplementedException();
	}

	public Task<bool> DeleteAnswerForMatch(long userid, long matchid)
	{
		throw new System.NotImplementedException();
	}

	public Task<bool> DeleteAnswerForStage(long userid, long stageid, string formid)
	{
		throw new System.NotImplementedException();
	}

	public async Task<IEnumerable<AnswersInGroupStage>> GetNumberOfAnswersPerUserInGroupStage(long userid)
	{
		return
		[
			new AnswersInGroupStage {
				GroupID = "A",
				PossibleAnswers = 2,
				GivenAnswers = 1
			},
			new AnswersInGroupStage {
				GroupID = "B",
				PossibleAnswers = 2,
				GivenAnswers = 2
			}
		];
	}

	public Task<IEnumerable<UserModel>> GetStandings()
	{
		throw new System.NotImplementedException();
	}

	public Task<IEnumerable<UserKnockoutModel>> GetUserKnockout(int userid)
	{
		throw new System.NotImplementedException();
	}

	public Task<IEnumerable<MatchModel>> GetUserMatches(int userid)
	{
		throw new System.NotImplementedException();
	}

	public Task<bool> IsCountryInStage(long userid, int stageid, string countryid)
	{
		throw new System.NotImplementedException();
	}

	public Task ResetPoint()
	{
		throw new System.NotImplementedException();
	}

	public Task SetPointsForGroupStage()
	{
		throw new System.NotImplementedException();
	}

	public Task SetPointsForKoStage()
	{
		throw new System.NotImplementedException();
	}
}
