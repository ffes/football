using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Football.Models;
using Football.Repositories;

#pragma warning disable CS1998	// Since we mock, the methods aren't actually async

namespace Football.Test.Mock;

public class MockMatchesRepository: IMatchesRepository
{
	public Task<DateTime> GetFirstKoMatchDate()
	{
		throw new NotImplementedException();
	}

	public Task<DateTime> GetFirstMatchDate()
	{
		throw new NotImplementedException();
	}

	public Task<IEnumerable<StageMatchModel>> GetKnockoutStage(long userid, int stageid)
	{
		throw new NotImplementedException();
	}

	public Task<IEnumerable<StageMatchModel>> GetKnockoutStage(int stageid)
	{
		throw new NotImplementedException();
	}

	public Task<IEnumerable<MatchModel>> GetKnockoutStageMatches()
	{
		throw new NotImplementedException();
	}

	public Task<IEnumerable<StageModel>> GetKnockoutStages(long userid)
	{
		throw new NotImplementedException();
	}

	public Task<MatchModel> GetMatchById(int matchid)
	{
		throw new NotImplementedException();
	}

	public Task<IEnumerable<MatchModel>> GetMatchesInGroup(long userid, string groupid)
	{
		throw new NotImplementedException();
	}

	public Task<IEnumerable<MatchModel>> GetMatchesInGroups()
	{
		throw new NotImplementedException();
	}

	public Task<bool> SetMatchAway(int matchid, string countryid)
	{
		throw new NotImplementedException();
	}

	public Task<bool> SetMatchHome(int matchid, string countryid)
	{
		throw new NotImplementedException();
	}

	public Task<bool> SetMatchResult(long matchid, int result)
	{
		throw new NotImplementedException();
	}
}
