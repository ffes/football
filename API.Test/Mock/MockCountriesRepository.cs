using System.Collections.Generic;
using System.Threading.Tasks;
using Football.Models;
using Football.Repositories;

#pragma warning disable CS1998	// Since we mock, the methods aren't actually async

namespace Football.Test.Mock;

public class MockCountriesRepository: ICountriesRepository
{
	public async Task<IEnumerable<CountryModel>> GetCountries()
	{
		return
		[
			new CountryModel {
				CountryID = "nl",
				CountryName = "Netherlands",
				GroupID = "A"
			},
			new CountryModel {
				CountryID = "de",
				CountryName = "Germany",
				GroupID = "A"
			},
			new CountryModel {
				CountryID = "be",
				CountryName = "Belgium",
				GroupID = "B"
			},
			new CountryModel {
				CountryID = "fr",
				CountryName = "France",
				GroupID = "B"
			}
		];
	}

	public Task<IEnumerable<CountryModel>> GetCountriesInGroups(string[] groups)
	{
		throw new System.NotImplementedException();
	}

	public Task<IEnumerable<CountryModel>> GetCountriesInStage(long userid, int stageid)
	{
		throw new System.NotImplementedException();
	}
}
