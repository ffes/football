export const environment = {
	production: true,
	enableTracing: false,
	apiRootUrl: '/api'
};
