import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, RouterModule } from '@angular/router';

import { UserKnockoutModel } from 'src/app/api/models';
import { PlayService } from 'src/app/api/services';

@Component({
	selector: 'app-user-ko',
	standalone: true,
	imports: [
		CommonModule,
		RouterModule,
	],
	templateUrl: './user-ko.component.html',
	styleUrl: './user-ko.component.scss'
})
export class UserKoComponent implements OnInit {

	public countries: UserKnockoutModel[] = [];
	public userid: number = 0;

	constructor(
		private playService: PlayService,
		private route: ActivatedRoute,
	) { }

	ngOnInit(): void {

		// Get the userid from the route
		this.userid = Number(this.route.snapshot.paramMap.get('userid'));

		// Get the predicted matches of this user
		this.playService.getUserKnockout({ userid: this.userid })
			.subscribe({
				next: (countries) => {
					this.countries = countries;
				},
			});
	}
}
