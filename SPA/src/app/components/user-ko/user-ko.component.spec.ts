import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserKoComponent } from './user-ko.component';

describe('UserKoComponent', () => {
	let component: UserKoComponent;
	let fixture: ComponentFixture<UserKoComponent>;
	
	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [UserKoComponent]
		})
		.compileComponents();
		
		fixture = TestBed.createComponent(UserKoComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});
	
	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
