import { Component, Input } from '@angular/core';
import { MatchModel } from 'src/app/api/models';
import { InputService, PlayService } from 'src/app/api/services';

@Component({
	selector: 'app-group-result-select',
	standalone: true,
	imports: [],
	templateUrl: './group-result-select.component.html',
	styleUrl: './group-result-select.component.scss'
})
export class GroupResultSelectComponent {

	@Input()
	public match: MatchModel = {};

	@Input()
	public admin: boolean = false;

	constructor(
		private inputService: InputService,
		private playService: PlayService,
	) { }

	public onSelected(val: string) {
		
		console.log(this.admin, this.match.matchID, val, this.match.answer);
		this.match.answer = parseInt(val);

		if (this.admin) {
			this.playService.patchMatchResult( { matchid: this.match.matchID || 0, body: this.match.answer } ).subscribe();
		}
		else {
			this.inputService.postAnswerForMatch( { matchid: this.match.matchID || 0, body: this.match.answer } ).subscribe();
		}
	}
}
