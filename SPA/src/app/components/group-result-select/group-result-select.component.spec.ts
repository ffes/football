import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupResultSelectComponent } from './group-result-select.component';

describe('ResultSelectComponent', () => {
	let component: GroupResultSelectComponent;
	let fixture: ComponentFixture<GroupResultSelectComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [GroupResultSelectComponent]
		})
		.compileComponents();

		fixture = TestBed.createComponent(GroupResultSelectComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
