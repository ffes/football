import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { GroupModel } from 'src/app/api/models';
import { InputService } from 'src/app/api/services';

@Component({
	selector: 'app-groups',
	standalone: true,
	imports: [
		CommonModule,
		RouterModule
	],
	templateUrl: './groups.component.html',
	styleUrl: './groups.component.scss'
})
export class GroupsComponent implements OnInit {

	public groups: GroupModel[] = [];

	constructor(private inputService: InputService) { }

	ngOnInit(): void {
		this.inputService.getGroups()
			.subscribe({
				next: (groups) => {
					console.log("groups", groups);
					this.groups = groups;
				},
			});
	}
}
