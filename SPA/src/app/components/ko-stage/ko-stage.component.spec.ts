import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KoStageComponent } from './ko-stage.component';

describe('KoStageComponent', () => {
	let component: KoStageComponent;
	let fixture: ComponentFixture<KoStageComponent>;
	
	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [KoStageComponent]
		})
		.compileComponents();
		
		fixture = TestBed.createComponent(KoStageComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});
	
	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
