import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

import { InputService } from 'src/app/api/services';
import { StageMatchModel } from 'src/app/api/models';
import { KoCountrySelectComponent } from 'src/app/components/ko-country-select/ko-country-select.component';

@Component({
	selector: 'app-ko-stage',
	standalone: true,
	imports: [
		CommonModule,
		KoCountrySelectComponent
	],
	templateUrl: './ko-stage.component.html',
	styleUrl: './ko-stage.component.scss'
})
export class KoStageComponent implements OnInit {

	public stageid: any;
	public stages: StageMatchModel[] = [];

	constructor(
		private inputService: InputService,
		private route: ActivatedRoute,
	) { }

	ngOnInit(): void {

		// Get the stageid from the route
		this.stageid = this.route.snapshot.paramMap.get('stageid');
		console.log("stageid", this.stageid);

		this.inputService.getKoStage({ stageid: this.stageid})
			.subscribe({
				next: (stages) => {
					console.log("stages", stages);
					this.stages = stages;
				},
			});
	}
}
