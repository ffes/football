import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, RouterModule } from '@angular/router';

import { MatchModel } from 'src/app/api/models';
import { PlayService } from 'src/app/api/services';

@Component({
	selector: 'app-user-groups',
	standalone: true,
	imports: [
		CommonModule,
		RouterModule,
	],
	templateUrl: './user-groups.component.html',
	styleUrl: './user-groups.component.scss'
})
export class UserGroupsComponent implements OnInit {

	public matches: MatchModel[] = [];
	public userid: number = 0;

	constructor(
		private playService: PlayService,
		private route: ActivatedRoute,
	) { }

	ngOnInit(): void {

		// Get the userid from the route
		this.userid = Number(this.route.snapshot.paramMap.get('userid'));

		// Get the predicted matches of this user
		this.playService.getUserGroups({ userid: this.userid })
			.subscribe({
				next: (matches) => {
					this.matches = matches;
				},
			});
	}
}
