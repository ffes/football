import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KoStagesComponent } from './ko-stages.component';

describe('KoStagesComponent', () => {
	let component: KoStagesComponent;
	let fixture: ComponentFixture<KoStagesComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [KoStagesComponent]
		})
		.compileComponents();

		fixture = TestBed.createComponent(KoStagesComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
