import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { InputService } from 'src/app/api/services';
import { StageModel } from 'src/app/api/models';

@Component({
	selector: 'app-ko-stages',
	standalone: true,
	imports: [
		CommonModule,
		RouterModule,
	],
	templateUrl: './ko-stages.component.html',
	styleUrl: './ko-stages.component.scss'
})
export class KoStagesComponent implements OnInit {

	public stages: StageModel[] = [];

	constructor(private inputService: InputService) { }

	ngOnInit(): void {
		this.inputService.getKoStages()
			.subscribe({
				next: (stages) => {
					console.log("stages", stages);
					this.stages = stages;
				},
			});
	}
}
