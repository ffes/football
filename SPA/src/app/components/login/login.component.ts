import {Component, OnInit} from '@angular/core';
import {FormGroup, NonNullableFormBuilder, ReactiveFormsModule, Validators} from '@angular/forms';
import {UsersService} from '../../api/services/users.service';
import {ApiModule} from '../../api/api.module';
import {SessionService} from '../../services/session.service';
import {Router, RouterLink} from '@angular/router';
import {map, switchMap, take} from 'rxjs/operators';
import {CommonModule} from '@angular/common';
import {Session} from 'protractor';
import {UserModel} from '../../api/models/user-model';

@Component({
	selector: 'app-login',
	standalone: true,
	templateUrl: './login.component.html',
	styleUrl: './login.component.scss',
	imports: [
		CommonModule,
		ReactiveFormsModule,
		RouterLink,
	]
})
export class LoginComponent implements OnInit {
	protected loginForm: FormGroup;
	protected error: any;

	constructor(
		private router: Router,
		private fb: NonNullableFormBuilder,
		private sessionService: SessionService,
	) {
		this.loginForm = fb.group({
			eMail: ['', Validators.required],
			password: ['', Validators.required]
		});
	}

	ngOnInit(): void {
		this.sessionService.session.pipe(
			take(1),
			map((session: UserModel|null) => !!session)
		)
			.subscribe({next: (hasSession: boolean) => {
				if (hasSession) {
					this.router.navigate(['/']);
				}
			} }) ;
	}

	public submit(): void {
		if ( this.loginForm.valid ) {
			this.error = undefined;

			this.sessionService.authenticate(this.loginForm.value)
				.pipe(
					switchMap(s => this.router.navigate(['/'] ))
				)
				.subscribe({
				next: () => {},
				error: (error) => this.error = error
			});
		}
	}
}
