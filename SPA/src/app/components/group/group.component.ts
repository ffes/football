import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

import { MatchModel } from 'src/app/api/models';
import { InputService } from 'src/app/api/services';
import { GroupResultSelectComponent } from 'src/app/components/group-result-select/group-result-select.component';

@Component({
	selector: 'app-group',
	standalone: true,
	imports: [
		CommonModule,
		GroupResultSelectComponent,
	],
	templateUrl: './group.component.html',
	styleUrl: './group.component.scss'
})
export class GroupComponent implements OnInit {

	public group: string = '';
	public matches: MatchModel[] = [];

	constructor(
		private inputService: InputService,
		private route: ActivatedRoute,
	) { }

	ngOnInit(): void {

		// Get the group from the route
		this.group = this.route.snapshot.paramMap.get('groupid') as string;

		// Get the matches of this group
		this.inputService.getGroup({ groupid: this.group })
			.subscribe({
				next: (matches) => {
					console.log("matches", matches);
					this.matches = matches;
				},
			});
	}
}
