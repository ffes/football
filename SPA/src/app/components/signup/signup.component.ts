import { Component } from '@angular/core';
import {FormGroup, NonNullableFormBuilder, ReactiveFormsModule, Validators} from '@angular/forms';
import {UsersService} from '../../api/services/users.service';
import {ApiModule} from '../../api/api.module';
import {SessionService} from '../../services/session.service';
import {Router, RouterLink} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {CommonModule, NgSwitch} from '@angular/common';

@Component({
	selector: 'app-login',
	standalone: true,
	templateUrl: './signup.component.html',
	styleUrl: './signup.component.scss',
	imports: [
		CommonModule,
		ReactiveFormsModule,
		RouterLink,
		NgSwitch,
	]
})
export class SignupComponent {
	protected signupForm: FormGroup;
	protected error: any;
	protected confirmation = false;


	constructor(
		private router: Router,
		private fb: NonNullableFormBuilder,
		private usersService: UsersService,
	) {
		this.signupForm = fb.group({
			realName: ['', Validators.required],
			nickName: ['', Validators.required],
			eMail: ['', Validators.required],
			password: ['', Validators.required]
		});
	}

	public submit(): void {
		if ( this.signupForm.valid ) {
			this.error = undefined;

			this.usersService.signUp({body: this.signupForm.value}).subscribe({
				next: (r) => this.confirmation = true,
				error: (error) => this.error = error
			});
		}
	}
}
