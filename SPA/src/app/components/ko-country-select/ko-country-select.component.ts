import { Component, OnInit, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InputService } from 'src/app/api/services';
import { CountryModel, StageMatchModel } from 'src/app/api/models';

@Component({
	selector: 'app-ko-country-select',
	standalone: true,
	imports: [
		CommonModule,
	],
	templateUrl: './ko-country-select.component.html',
	styleUrl: './ko-country-select.component.scss'
})
export class KoCountrySelectComponent implements OnInit {

	@Input()
	public match: StageMatchModel = {};
	public countries: CountryModel[] = [];

	constructor(private inputService: InputService) { }

	ngOnInit(): void {

		const stageID = this.match.stageID || 0;
		if (stageID > 2)		// TODO: Magic Number Alert!!!
		{
			this.inputService.getCountriesFromStage({ stageid: stageID - 1})
				.subscribe({
					next: (countries) => {
						console.log("countries", countries);
						this.countries = countries;
					},
				});
		}
		else
		{
			this.inputService.getCountriesFrom({ from: this.match.from ?? "" })
			.subscribe({
				next: (countries) => {
					console.log("countries", countries);
					this.countries = countries;
				},
			});
		}
	}

	public onSelected(val: string) {
		console.log(val);
		this.inputService.postAnswerForStage( { stageid: this.match.stageID || 0, body: { countryID: val, formID: this.match.formID } } ).subscribe();
	}
}
