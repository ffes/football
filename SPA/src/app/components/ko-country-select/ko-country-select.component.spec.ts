import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KoCountrySelectComponent } from './ko-country-select.component';

describe('KoCountrySelectComponent', () => {
	let component: KoCountrySelectComponent;
	let fixture: ComponentFixture<KoCountrySelectComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [KoCountrySelectComponent]
		})
		.compileComponents();

		fixture = TestBed.createComponent(KoCountrySelectComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
