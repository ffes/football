import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StageModel } from 'src/app/api/models';
import { InputService } from 'src/app/api/services';

@Component({
	selector: 'app-reglement',
	standalone: true,
	imports: [
  		CommonModule,
	],
	templateUrl: './reglement.component.html',
	styleUrl: './reglement.component.scss'
})
export class ReglementComponent implements OnInit {

	public stages: StageModel[] = [];

	constructor(private inputService: InputService) { }

	ngOnInit(): void {
		this.inputService.getKoStages()
			.subscribe({
				next: (stages) => {
					console.log("stages", stages);
					this.stages = stages;
				},
			});
	}
}
