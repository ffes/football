import { Component } from "@angular/core";
import { RouterModule } from '@angular/router';

import { GroupsComponent } from "src/app/components/groups/groups.component";
import { KoStagesComponent } from "src/app/components/ko-stages/ko-stages.component";

@Component({
	selector: 'app-input',
	standalone: true,
	templateUrl: './input.component.html',
	styleUrl: './input.component.scss',
	imports: [
		RouterModule,
		GroupsComponent,
		KoStagesComponent
	]
})
export class InputComponent {
}
