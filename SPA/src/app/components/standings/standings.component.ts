import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { UserModel } from 'src/app/api/models';
import { PlayService } from 'src/app/api/services';

@Component({
	selector: 'app-standings',
	standalone: true,
	imports: [
		CommonModule,
		RouterModule,
	],
	templateUrl: './standings.component.html',
	styleUrl: './standings.component.scss'
})
export class StandingsComponent implements OnInit {

	public users: UserModel[] = [];
	public inKO: boolean = false;

	constructor(private playService: PlayService) { }

	ngOnInit(): void {

		this.playService.inKoStages().subscribe({
			next: (inKO) => {
				this.inKO = inKO;
			},
		});

		this.playService.getStandings()
			.subscribe({
				next: (users) => {
					this.users = users;
				},
			});
	}
}
