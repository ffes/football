import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminComponent } from './admin.component';
import { AdminHomeComponent } from './components/home/home.component';
import { ResultsGroupsComponent } from './components/results-groups/results-groups.component';
import { ResultsKoComponent } from './components/results-ko/results-ko.component';
import { UsersComponent } from './components/users/users.component';

const routes: Routes = [{
	path: '',
	component: AdminComponent,
	children: [
		{ path: '', component: AdminHomeComponent },
		{ path: 'users', component: UsersComponent },
		{ path: 'results/groups', component: ResultsGroupsComponent },
		{ path: 'results/ko', component: ResultsKoComponent },
	]
}];

@NgModule({
	imports: [
		RouterModule.forChild(routes),
	],
	exports: [
		RouterModule,
	],
})
export class AdminRoutingModule { }
