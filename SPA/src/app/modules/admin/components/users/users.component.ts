import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Observable } from 'rxjs';

import { UsersService } from 'src/app/api/services/users.service';
import { UserModel } from 'src/app/api/models/user-model';

@Component({
	selector: 'app-users',
	standalone: true,
	imports: [
		CommonModule
	],
	templateUrl: './users.component.html',
	styleUrl: './users.component.scss'
})
export class UsersComponent implements OnInit {
	protected users: Observable<UserModel[]> | undefined;

	constructor(public usersService: UsersService) { }

	ngOnInit(): void {
		this.refresh();
	}

	private refresh(): void {
		this.users = this.usersService.getUsers();
	}

	public enableUser(userid: number, enable: boolean): void {
		if ( !userid ) {
			return;
		}

		if ( enable ) {
			this.usersService.enableUser({id: userid}).subscribe({next: () => this.refresh() });
		}
		else {
			this.usersService.disableUser({id: userid}).subscribe({next: () => this.refresh() });
		}
	}
}
