import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatchModel } from 'src/app/api/models';
import { PlayService } from 'src/app/api/services';
import { GroupResultSelectComponent } from 'src/app/components/group-result-select/group-result-select.component';

@Component({
	selector: 'app-results-groups',
	standalone: true,
	imports: [
		CommonModule,
		GroupResultSelectComponent,
	],
	templateUrl: './results-groups.component.html',
	styleUrl: './results-groups.component.scss'
})
export class ResultsGroupsComponent implements OnInit {

	public matches: MatchModel[] = [];

	constructor(
		private playService: PlayService,
	) { }

	ngOnInit(): void {

		// Get the matches
		this.playService.getMatchesForGroups()
			.subscribe({
				next: (matches) => {
					console.log("matches", matches);
					this.matches = matches;
				},
			});
	}
}
