import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsGroupsComponent } from './results-groups.component';

describe('ResultsGroupsComponent', () => {
	let component: ResultsGroupsComponent;
	let fixture: ComponentFixture<ResultsGroupsComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [ResultsGroupsComponent]
		})
		.compileComponents();

		fixture = TestBed.createComponent(ResultsGroupsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
