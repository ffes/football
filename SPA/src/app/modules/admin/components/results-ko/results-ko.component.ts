import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatchModel } from 'src/app/api/models';
import { PlayService } from 'src/app/api/services';
import { AdminKoCountrySelectComponent } from '../admin-ko-country-select/admin-ko-country-select.component';

@Component({
	selector: 'app-results-ko',
	standalone: true,
	imports: [
		CommonModule,
		AdminKoCountrySelectComponent,
	],
	templateUrl: './results-ko.component.html',
	styleUrl: './results-ko.component.scss'
})
export class ResultsKoComponent implements OnInit {

	public matches: MatchModel[] = [];

	constructor(
		private playService: PlayService,
	) { }

	ngOnInit(): void {

		// Get the matches
		this.playService.getMatchesForKnockout()
			.subscribe({
				next: (matches) => {
					console.log("matches", matches);
					this.matches = matches;
				},
			});
	}
}
