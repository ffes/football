import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsKoComponent } from './results-ko.component';

describe('ResultsKoComponent', () => {
	let component: ResultsKoComponent;
	let fixture: ComponentFixture<ResultsKoComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [ResultsKoComponent]
		})
		.compileComponents();

		fixture = TestBed.createComponent(ResultsKoComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
