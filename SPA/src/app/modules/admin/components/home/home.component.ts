import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { PlayService } from 'src/app/api/services';

@Component({
	selector: 'app-admin-home',
	standalone: true,
	imports: [
		RouterModule,
		CommonModule,
	],
	templateUrl: './home.component.html',
	styleUrl: './home.component.scss'
})
export class AdminHomeComponent {

	constructor(
		private playService: PlayService,
	) { }

	public recalcScores() {
		this.playService.recalcStandings().subscribe();
	}
}
