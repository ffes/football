import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminKoCountrySelectComponent } from './admin-ko-country-select.component';

describe('AdminKoCountrySelectComponent', () => {
	let component: AdminKoCountrySelectComponent;
	let fixture: ComponentFixture<AdminKoCountrySelectComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [AdminKoCountrySelectComponent]
		})
		.compileComponents();

		fixture = TestBed.createComponent(AdminKoCountrySelectComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
