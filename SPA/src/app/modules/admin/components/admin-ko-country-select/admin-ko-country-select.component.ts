import { Component, OnInit, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlayService, InputService } from 'src/app/api/services';
import { CountryModel, MatchModel } from 'src/app/api/models';

@Component({
	selector: 'app-admin-ko-country-select',
	standalone: true,
	imports: [
		CommonModule,
	],
	templateUrl: './admin-ko-country-select.component.html',
	styleUrl: './admin-ko-country-select.component.scss'
})
export class AdminKoCountrySelectComponent implements OnInit {

	@Input()
	public match: MatchModel = {};

	@Input()
	public home: boolean = true;

	public countries: CountryModel[] = [];

	constructor(
		private playService: PlayService,
		private inputService: InputService,
	) { }

	ngOnInit(): void {

		console.log(this.match);
		const stageID = this.match.stageID || 0;
		this.inputService.getCountriesFrom({ from: (this.home ? this.match.homeFrom : this.match.awayFrom) ?? "" })
		.subscribe({
			next: (countries) => {
				console.log("countries", countries);
				this.countries = countries;
			},
		});
	}

	public onSelected(val: string) {

		console.log(val);
		if (this.home)
		{
			this.playService.patchMatchHome({ matchid: this.match.matchID || 0, country: val }).subscribe();
		}
		else
		{
			this.playService.patchMatchAway({ matchid: this.match.matchID || 0, country: val }).subscribe();
		}
	}
}
