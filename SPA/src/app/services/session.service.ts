import {Injectable} from '@angular/core';
import { Observable, ReplaySubject} from 'rxjs';
import {UsersService} from '../api/services/users.service';
import {UserModel} from '../api/models/user-model';
import {AuthenticateModel} from '../api/models/authenticate-model';
import {tap} from 'rxjs/operators';

@Injectable({
	providedIn: 'root',
})
export class SessionService {
	private sessionSubject = new ReplaySubject<UserModel|null>(1);

	public get session(): Observable<UserModel|null> {
		return this.sessionSubject.asObservable();
	}

	constructor(protected usersService: UsersService) {
		this.usersService.activate().subscribe({
			next: (model) => this.sessionSubject.next(model),
			error: (error) => {
				console.warn('Error restoring session:', error);
				this.sessionSubject.next(null);
			},
		});
	}

	public authenticate(credentials: AuthenticateModel): Observable<UserModel> {
		return this.usersService.authenticate({body: credentials}).pipe(
			tap((s) => {
				console.log('Activating session:', s);
				this.sessionSubject.next(s);
			})
		);
	}

	public logout(): Observable<void> {
		return this.usersService.signout().pipe(
			tap((r) => this.sessionSubject.next(null)),
		);
	}

}
