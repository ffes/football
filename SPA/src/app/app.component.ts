import {Component, signal} from '@angular/core';
import {SessionService} from './services/session.service';
import {Router} from '@angular/router';
import {UserModel} from './api/models/user-model';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {
	title = 'football';
	public session: UserModel | null = null;

	constructor(
		private sessionService: SessionService,
		private router: Router,
	) {
		this.sessionService.session.subscribe({next: (s) => this.session = s });
	}

	public signout(e: Event): boolean {
		this.sessionService.logout().subscribe({
			next: () => this.router.navigate(['/login'])
		});

		e.preventDefault();

		return false;
	}
}
