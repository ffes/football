import {ActivatedRouteSnapshot, CanActivateFn, CanMatchFn, RouterStateSnapshot} from '@angular/router';
import {SessionService} from '../services/session.service';
import {inject} from '@angular/core';
import {map, take} from 'rxjs/operators';
import {InputService} from '../api/services';

export const inputAllowedCanActivateFunction: CanActivateFn =
	( next: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
		const inputService = inject(InputService);

		return inputService.inputAllowed();
	};

export const inputAllowedCanMatchFunction: CanMatchFn =
	( route, segments) => {
		const inputService = inject(InputService);

		return inputService.inputAllowed();
	};

export const inputNotAllowedCanActivateFunction: CanActivateFn =
	( next: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
		const inputService = inject(InputService);

		return inputService.inputAllowed().pipe(
			map((r) => !r)
		);
	};

export const inputNotAllowedCanMatchFunction: CanMatchFn =
	( route, segments) => {
		const inputService = inject(InputService);

		return inputService.inputAllowed().pipe(
			map((r) => !r)
		);
	};


