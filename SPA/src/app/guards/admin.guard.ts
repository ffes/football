import {ActivatedRouteSnapshot, CanActivateFn, CanMatchFn, RouterStateSnapshot} from '@angular/router';
import {SessionService} from '../services/session.service';
import {inject} from '@angular/core';
import {map, take} from 'rxjs/operators';

export const adminRoleCanActivateFunction: CanActivateFn =
	( next: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
		const sessionService = inject(SessionService);

		return sessionService.session.pipe(
			take(1),
			map(s => !!s?.userLevel && s.userLevel >= 100),
		);
	};



export const adminRoleCanMatchFunction: CanMatchFn =
	( route, segments) => {
		const sessionService = inject(SessionService);

		return sessionService.session.pipe(
			take(1),
			map(s => !!s?.userLevel && s.userLevel >= 100),
		);
	};


