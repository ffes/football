import {ActivatedRouteSnapshot, CanActivateFn, CanMatchFn, RouterStateSnapshot} from '@angular/router';
import {SessionService} from '../services/session.service';
import {inject} from '@angular/core';
import {map, take} from 'rxjs/operators';

export const sessionCanActivateFunction: CanActivateFn =
	( next: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
		const sessionService = inject(SessionService);

		return sessionService.session.pipe(
			take(1),
			map(s => !!s),
		);
	};



export const sessionCanMatchFunction: CanMatchFn =
	( route, segments) => {
		const sessionService = inject(SessionService);

		return sessionService.session.pipe(
			take(1),
			map(s => !!s),
		);
	};


