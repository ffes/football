import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GroupComponent } from './components/group/group.component';
import { InputComponent } from './components/input/input.component';
import { KoStageComponent } from './components/ko-stage/ko-stage.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { ReglementComponent } from './components/reglement/reglement.component';
import { StandingsComponent } from './components/standings/standings.component';
import { UserGroupsComponent } from './components/user-groups/user-groups.component';
import { UserKoComponent } from './components/user-ko/user-ko.component';

import { sessionCanActivateFunction, sessionCanMatchFunction } from './guards/auth.guard';
import { environment } from 'src/environments/environment';
import { inputAllowedCanMatchFunction, inputNotAllowedCanMatchFunction } from './guards/input.guard';
import { adminRoleCanActivateFunction, adminRoleCanMatchFunction } from './guards/admin.guard';

const routes: Routes = [
	{
		path: '',
		canActivate: [ sessionCanActivateFunction ],
		canMatch: [ sessionCanMatchFunction ],
		children: [
			{ path: 'input', canMatch: [ inputAllowedCanMatchFunction ], component: InputComponent },
			{ path: 'group/:groupid', canMatch: [ inputAllowedCanMatchFunction ], component: GroupComponent },
			{ path: 'ko-stage/:stageid', canMatch: [ inputAllowedCanMatchFunction ], component: KoStageComponent },
			{ path: 'standings', canMatch: [ inputNotAllowedCanMatchFunction ], component: StandingsComponent },
			{ path: 'play/user/:userid/groups', canMatch: [ inputNotAllowedCanMatchFunction ], component: UserGroupsComponent },
			{ path: 'play/user/:userid/ko', canMatch: [ inputNotAllowedCanMatchFunction ], component: UserKoComponent },
			{ path: '', canMatch: [ inputAllowedCanMatchFunction ], redirectTo: 'input', pathMatch: 'full' },
			{ path: '', canMatch: [ inputNotAllowedCanMatchFunction ], redirectTo: 'standings', pathMatch: 'full' },
		],
	},
	{
		path: 'admin',
		canMatch: [ adminRoleCanMatchFunction ],
		canActivate: [ adminRoleCanActivateFunction ],
		loadChildren: () => import('./modules/admin/admin.module').then(m => m.AdminModule)
	},
	{ path: 'reglement', component: ReglementComponent },
	{ path: 'login', component: LoginComponent },
	{ path: 'signup', component: SignupComponent },
	{ path: '', redirectTo: '/login', pathMatch: 'full' },
	{ path: '**', redirectTo: '/login' },
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes, { enableTracing: environment.enableTracing })
	],
	exports: [
		RouterModule
	]
})
export class AppRoutingModule { }
