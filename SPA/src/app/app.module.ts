import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ApiModule } from './api/api.module';
import { environment } from 'src/environments/environment';

@NgModule({
	declarations: [
		AppComponent,
	],
	providers: [],
	bootstrap: [
		AppComponent
	],
	imports: [
		BrowserModule,
		ApiModule.forRoot({ rootUrl: environment.apiRootUrl }),
		HttpClientModule,
		AppRoutingModule
	]
})
export class AppModule { }
